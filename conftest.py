from datetime import datetime

import pytest
from _pytest.terminal import TerminalReporter


"""
Solution largely based on pytest-timestamper module. [1]
On pytest-4.6.11 (available on CC7), verbose mode is required
as "_locationline" is only called during verbose mode. 

[1] https://github.com/mbkroese/pytest-timestamper
"""


class TimestampTerminalReporter(TerminalReporter):
    def __init__(self, config):
        TerminalReporter.__init__(self, config)
        self.cache = {}

    def _locationline(self, nodeid, fspath, lineno, domain):
        key = (nodeid, fspath, lineno, domain)
        if key not in self.cache:
            default = TerminalReporter._locationline(self, nodeid, fspath, lineno, domain)
            datestring = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            self.cache[key] = "[{}] {}".format(datestring, default)
        return self.cache[key]


@pytest.mark.trylast
def pytest_configure(config):
    reporter = config.pluginmanager.get_plugin("terminalreporter")
    config.pluginmanager.unregister(reporter)
    config.pluginmanager.register(TimestampTerminalReporter(config), "terminalreporter")
