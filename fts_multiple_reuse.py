#!/usr/bin/env python3
import errno
import unittest

from lib import TestCaseBase
from lib import mock_plugin_helper as mock


class TestMultipleReuse(TestCaseBase):
    """
    Test job with multiple transfers and reuse enabled
    """

    NPAIRS = 5

    def test_multiple_with_reuse(self):
        """
        Transfer multiple files with reuse
        """
        transfers = []
        for i in range(self.NPAIRS):
            src, dst = mock.generate_pair(source_host='test.cern.ch', dest_host='test.somewhere.uk')
            transfers.append({'sources': [src], 'destinations': [dst]})

        job_id = self.client.submit(transfers, reuse=True)
        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        for f in job['files']:
            self.assertEqual('FINISHED', f['file_state'])

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id, expected=self.NPAIRS)
            self.assertEqual(self.NPAIRS, len(job_msgs))
            for msg in job_msgs:
                self.assertEqual('Ok', msg['t_final_transfer_state'])

    def test_multiple_with_reuse_dirty(self):
        """
        Transfer multiple files with reuse, one does not exist
        """
        transfers = []
        for i in range(self.NPAIRS):
            src, dst = mock.generate_pair(source_host='test.cern.ch', dest_host='test.somewhere.uk')
            transfers.append({'sources': [src], 'destinations': [dst]})

        # Replace last one with a enoent
        bad_src = mock.generate_url(host='test.cern.ch', errno=errno.ENOENT)
        transfers[-1]['sources'] = [bad_src]

        job_id = self.client.submit(transfers, reuse=True)
        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHEDDIRTY', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        for f in job['files']:
            if f['source_surl'] == bad_src:
                self.assertEqual('FAILED', f['file_state'])
            else:
                self.assertEqual('FINISHED', f['file_state'])

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id, expected=self.NPAIRS)
            self.assertEqual(self.NPAIRS, len(job_msgs))
            for msg in job_msgs:
                if bad_src == msg['src_url']:
                    self.assertEqual('Error', msg['t_final_transfer_state'])
                else:
                    self.assertEqual('Ok', msg['t_final_transfer_state'])

    def test_multiple_with_reuse_dirty2(self):
        """
        Same as before, but let the first fail. Error message should not
        affect other transfers
        """
        transfers = []
        for i in range(self.NPAIRS):
            src, dst = mock.generate_pair(source_host='test.cern.ch', dest_host='test.somewhere.uk')
            transfers.append({'sources': [src], 'destinations': [dst]})

        # Replace last one with a enoent
        bad_src = mock.generate_url(host='test.cern.ch', errno=errno.ENOENT)
        transfers[0]['sources'] = [bad_src]

        job_id = self.client.submit(transfers, reuse=True)
        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHEDDIRTY', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        for f in job['files']:
            if f['source_surl'] == bad_src:
                self.assertEqual('FAILED', f['file_state'])
            else:
                self.assertEqual('FINISHED', f['file_state'])

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id, expected=self.NPAIRS)
            self.assertEqual(self.NPAIRS, len(job_msgs))
            for msg in job_msgs:
                if bad_src == msg['src_url']:
                    self.assertEqual('Error', msg['t_final_transfer_state'])
                else:
                    self.assertEqual('Ok', msg['t_final_transfer_state'])

    @TestCaseBase.skip_for_short_run
    def test_multiple_reuse_with_retries(self):
        """
        Submit a reuse job with retry enabled. Some will fail and be retried, others succeed.
        All of them must reach a terminal state.
        Regression for FTS-339
        """
        pairs = []
        pairs.append(mock.generate_pair(source_host='test.cern.ch', dest_host='test.somewhere.uk'))
        pairs.append(mock.generate_pair(source_host='test.cern.ch', dest_host='test.somewhere.uk'))
        src, bad_dst = mock.generate_pair(
            source_host='test.cern.ch', dest_host='test.somewhere.uk', transfer_errno=errno.EAGAIN
        )
        pairs.append((src, bad_dst))
        transfers = []
        for src, fail_dst in pairs:
            transfers.append({'sources': [src], 'destinations': [fail_dst]})

        job_id = self.client.submit(transfers, reuse=True, retry=2, retry_delay=10)
        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHEDDIRTY', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        for f in job['files']:
            if f['dest_surl'] == bad_dst:
                self.assertEqual('FAILED', f['file_state'])
                retries = self.client.get_retries(job_id, f['file_id'])
                self.assertEqual(2, len(retries))
            else:
                self.assertEqual('FINISHED', f['file_state'])
                retries = self.client.get_retries(job_id, f['file_id'])
                self.assertEqual(0, len(retries))

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id)
            retry_count = 0
            retries = []
            for msg in job_msgs:
                if bad_dst == msg['dst_url']:
                    self.assertEqual('Error', msg['t_final_transfer_state'])
                    retry_count += 1
                    retry = int(msg['retry'])
                    self.assertNotIn(retry, retries)
                    retries.append(retry)
                else:
                    self.assertEqual('Ok', msg['t_final_transfer_state'])
                    self.assertEqual(0, int(msg['retry']))

            self.assertEqual(max(retries), 2)


if __name__ == '__main__':
    unittest.main()
