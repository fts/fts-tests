#!/usr/bin/env python3
import subprocess
import time
import unittest

from lib import TestCaseBase
from lib import mock_plugin_helper as mock
from fts3.rest.client.exceptions import *


class TestBan(TestCaseBase):
    """
    Test simple submissions. Just a source and a destination.
    """

    BAN_SE = 'ban.cern.ch'

    def tearDown(self):
        super(TestBan, self).tearDown()
        self.client.unban_se('mock://' + self.BAN_SE)

    @TestCaseBase.skip_for_short_run
    def test_ban_src_se(self):
        """
        Ban source SE, submission must be denied
        """
        self.client.ban_se('mock://' + self.BAN_SE)

        src, dst = mock.generate_pair(source_host=self.BAN_SE)
        try:
            self.client.submit([{'sources': [src], 'destinations': [dst]}])
            self.fail('Submission should have failed')
        except (Unauthorized, Exception):
            pass

    @TestCaseBase.skip_for_short_run
    def test_ban_dst_se(self):
        """
        Ban destination SE, submission must be denied
        """
        self.client.ban_se('mock://' + self.BAN_SE)

        src, dst = mock.generate_pair(dest_host=self.BAN_SE)
        try:
            self.client.submit([{'sources': [src], 'destinations': [dst]}])
            self.fail('Submission should have failed')
        except (Unauthorized, Exception):
            pass

    @TestCaseBase.skip_for_short_run
    def test_ban_while_running(self):
        """
        Submitting the transfer while it stays in the queue
        Then we will try to ban it with the status CANCEL
        """
        src, dst = mock.generate_pair(dest_host=self.BAN_SE, transfer_time='5s')
        job_id = self.client.submit([{'sources': [src], 'destinations': [dst]}])

        self.client.ban_se('mock://' + self.BAN_SE, status='cancel')

        job = self.client.wait_for(job_id)
        self.assertEqual('CANCELED', job['job_state'])

    @TestCaseBase.skip_for_short_run
    def test_wait_while_running(self):
        """
        Submitting the transfer while it stays in the queue
        Than we will try to ban it with the status WAIT
        """
        src, dst = mock.generate_pair(dest_host=self.BAN_SE, transfer_time='1s')
        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            retry=1, retry_delay=5
        )

        self.client.ban_se('mock://' + self.BAN_SE, status='wait')

        time.sleep(2)
        job = self.client.get(job_id)
        self.assertEqual('SUBMITTED', job['job_state'])

        time.sleep(1)
        job = self.client.get(job_id)
        self.assertEqual('SUBMITTED', job['job_state'])

        self.client.unban_se('mock://' + self.BAN_SE)
        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

    @TestCaseBase.skip_for_short_run
    def test_wait_while_running_staging(self):
        """
        Same as before, but for a staging job
        """
        src, dst = mock.generate_pair(source_host=self.BAN_SE)
        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            bring_online=180
        )
        self.client.ban_se('mock://' + self.BAN_SE, status='wait')
        job = self.client.get(job_id, list_files=True)
        self.assertEqual('ON_HOLD_STAGING', job['files'][0]['file_state'])

        self.client.unban_se('mock://' + self.BAN_SE)
        job = self.client.get(job_id, list_files=True)
        self.assertEqual('STAGING', job['files'][0]['file_state'])

        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

    def _getSelfDn(self):
        """
        Get our own DN
        """
        proc = subprocess.Popen(['voms-proxy-info', '-identity'], stdout=subprocess.PIPE)
        self.assertEqual(0, proc.wait())
        return proc.stdout.readline().strip()

    @TestCaseBase.skip_for_short_run
    def test_ban_dn(self):
        """
        Ban a user DN
        """
        self.client.ban_dn('/DC=ch/DC=cern/CN=fake')
        self.client.unban_dn('/DC=ch/DC=cern/CN=fake')

        try:
            self_dn = self._getSelfDn()
            self.client.ban_dn(self_dn)
            self.fail('Shouldn\'t have been able to ban self!')
        except:
            pass


if __name__ == '__main__':
    unittest.main()
