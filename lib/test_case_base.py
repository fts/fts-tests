import logging
import os
import time
import unittest

import lib.config as config
from lib.messaging import FTSConsumer

ClientImplMod = __import__(config.ClientImpl, fromlist=['ClientImpl'])
ClientImpl = getattr(ClientImplMod, 'ClientImpl')

log = logging.getLogger(__name__)


class MessagingWrapper(FTSConsumer):
    def __init__(self, *args, **kwargs):
        self.vo_name = kwargs.pop('vo', None)
        super(MessagingWrapper, self).__init__(*args, **kwargs)
        self.start_messages = dict()
        self.completion_messages = dict()
        self.state_messages = dict()
        self.state_index = 0
        self.target_job = None

    def on_start_msg(self, msg):
        if msg['vo'] != self.vo_name:
            return
        _, _, _, fileid, jobid = msg['transfer_id'].split('__')
        if not self.target_job or self.target_job == jobid:
            if jobid not in self.start_messages:
                self.start_messages[jobid] = list()
            self.start_messages[jobid].append(msg)

    def on_complete_msg(self, msg):
        if msg['vo'] != self.vo_name:
            return
        _, _, _, fileid, jobid = msg['tr_id'].split('__')
        if not self.target_job or self.target_job == jobid:
            if jobid not in self.completion_messages:
                self.completion_messages[jobid] = list()
            self.completion_messages[jobid].append(msg)

    def on_state_msg(self, msg):
        if msg['vo_name'] != self.vo_name:
            return
        jobid = msg['job_id']
        fileid = int(msg['file_id'])
        if not self.target_job or self.target_job == jobid:
            if (jobid, fileid) not in self.state_messages:
                self.state_messages[(jobid, fileid)] = list()
            msg['order'] = self.state_index
            self.state_messages[(jobid, fileid)].append(msg)
            self.state_index += 1

    def clear(self):
        """
        Clear message lists
        """
        self.completion_messages = dict()
        self.start_messages = dict()
        self.state_messages = dict()
        self.state_index = 0

    def get_job_start(self, jobid, timeout=30, expected=1):
        """
        Get the start message for the given jobid
        If not in the list, do not wait more than timeout seconds
        """
        log.info('Get start messages for %s with timeout %d' % (jobid, timeout))
        self.target_job = jobid
        result = list()
        while timeout > 0 and len(result) < expected:
            if jobid in self.start_messages:
                result = self.start_messages[jobid]
                if len(result) >= expected:
                    break
                else:
                    log.info('Got %d messages, expecting %d' % (len(result), expected))
            timeout -= 5
            time.sleep(5)
        log.info("%d start messages for %s" % (len(result), jobid))
        self.target_job = None
        return result

    def get_job_completion(self, jobid, timeout=30, expected=1):
        """
        Get the completion message for the given job_id
        If not in the list, do not wait more than timeout seconds
        """
        log.info('Get stop messages for %s with timeout %d' % (jobid, timeout))
        self.target_job = jobid
        result = list()
        while timeout > 0 and len(result) < expected:
            if jobid in self.completion_messages:
                result = self.completion_messages[jobid]
                if len(result) >= expected:
                    break
            timeout -= 5
            time.sleep(5)
        log.info("%d completion messages for %s" % (len(result), jobid))
        self.target_job = None
        return result

    def get_state_msg(self, jobid, fileid, timeout=30, expected=1):
        """
        Get state change messages
        """
        log.info('Get state messages for %s with timeout %d' % (jobid, timeout))
        self.target_job = jobid
        result = list()
        while timeout > 0 and len(result) < expected:
            if (jobid, fileid) in self.state_messages:
                result = self.state_messages[(jobid, fileid)]
                if len(result) >= expected:
                    break
            timeout -= 5
            time.sleep(5)
        log.info("%d state messages for %s" % (len(result), jobid))
        self.target_job = None
        return result
    
    

if config.MsgEnable:
    fts_consumer = MessagingWrapper(
        vo=config.Vo,
        alias=config.MsgBrokerHost, port=config.MsgBrokerPort,
        cert=config.UserCert, key=config.UserKey, password=config.Password,
        start_topic=config.MsgStart, complete_topic=config.MsgComplete,
        state_topic=config.MsgState
    )
else:
  fts_consumer = None 


class TestCaseBase(unittest.TestCase):
    """
    To be used by tests as a base class, as it already sets up some
    common fields
    """

    @classmethod
    def skip_for_short_run(cls, method):
        """
        To be used as a decorator of long-running tests.
        This allows to disable them selectively
        """
        if os.environ.get('SHORT_RUN', 0):
            return unittest.skip("Short run")(method)
        return method

    def __init__(self, *args, **kwargs):
        super(TestCaseBase, self).__init__(*args, **kwargs)
        self.client = ClientImpl(
            endpoint=config.Fts3Endpoint, poll_interval=config.PollInterval,
            timeout=config.Timeout, label=config.TestLabel
        )

    @classmethod
    def setUpClass(cls):
        cls.messaging = fts_consumer
        if cls.messaging:
            cls.messaging.start()

    @classmethod
    def tearDownClass(cls):
        if cls.messaging:
            cls.messaging.stop()

    def setUp(self):
        pass

    def tearDown(self):
        if self.messaging:
            self.messaging.clear()

    # Patch a couple of methods that may not be available in TestCase (el6)
    def assertIn(self, member, container, msg=None):
        if member not in container:
            standard_msg = '%s not found in %s' % (unittest.util.safe_repr(member),
                                                   unittest.util.safe_repr(container))
            self.fail(self._formatMessage(msg, standard_msg))

    def assertLessEqual(self, a, b, msg=None):
        if not a <= b:
            standard_msg = '%s not less than or equal to %s' % (unittest.util.safe_repr(a), unittest.util.safe_repr(b))
            self.fail(self._formatMessage(msg, standard_msg))

    def assertGreater(self, a, b, msg=None):
        if not a > b:
            standard_msg = '%s not greater than %s' % (unittest.util.safe_repr(a), unittest.util.safe_repr(b))
            self.fail(self._formatMessage(msg, standard_msg))

    def assertStateTransitions(self, job_id, file_id, *args):
        state_msg = self.messaging.get_state_msg(job_id, file_id, expected=len(args))
        states = list(map(
            lambda m: str(m['file_state']),
            sorted(state_msg, key=lambda m: (int(m['timestamp']), m['order']))
        ))
        if set(args) != set(states):
            standard_msg = 'Different sets: %s != %s' % (args, states)
            self.fail(standard_msg)
    
        
    def assertStagingTransitions(self, job_id, file_id, *args):
        state_msg = self.messaging.get_state_msg(job_id, file_id, expected=len(args))
        states_staging = list(map(
            lambda m: str(m['file_state']),
            filter(
                lambda m: 'staging' in m,
                sorted(state_msg, key=lambda m: (int(m['timestamp']), m['order']))
            )
        ))
        if any(v is False for v in states_staging):
            standard_msg = 'Some states are not staging when they should: %s != %s' % (set(args), set(states_staging))
            self.fail(standard_msg)
            
    def assertStagingFileSize(self, job_id, file_id, *args):
        state_msg = self.messaging.get_state_msg(job_id, file_id, expected=len(args))
        states_staging = list(map(
            lambda m: str(m['file_state']),
            filter(
                lambda m: 'user_filesize' in m,
                sorted(state_msg, key=lambda m: (int(m['timestamp']), m['order']))
            )
        ))
        if any(v is 0 for v in states_staging):
            standard_msg = 'Some staging states are not having the same filesize they should: %s != %s' % (set(args), set(states_staging))
            self.fail(standard_msg)

    def assertArchivingTransitions(self, job_id, file_id, *args):
        state_msg = self.messaging.get_state_msg(job_id, file_id, expected=len(args))
        states_archiving = list(map(
            lambda m: str(m['file_state']),
            filter(
                lambda m: 'archiving' in m,
                sorted(state_msg, key=lambda m: (int(m['timestamp']), m['order']))
            )
        ))
        if any(v is False for v in states_archiving):
            standard_msg = 'Some states are not archiving when they should: %s != %s' % (set(args), set(states_archiving))
            self.fail(standard_msg)
