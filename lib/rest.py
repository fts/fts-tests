import inspect
import json
import logging
import time
from fts3.rest.client.request import Request
import fts3.rest.client.easy as fts3


log = logging.getLogger(__name__)


class ClientImpl(object):
    """
    Wrap the REST client with some additional useful methods
    """

    def __init__(self, endpoint, poll_interval=2, timeout=900, label='fts3-tests'):
        
        self.endpoint = endpoint
        self.context = fts3.Context(endpoint=self.endpoint)
        self.poll_interval = poll_interval
        self.timeout = timeout
        self.label = label

    def submit(self, transfers=None, deletion=None, **kwargs):
        """
        Submit a new job
        """
        # Label
        caller = inspect.stack()[1][3]
        labeldict = {'label': self.label, 'test': caller}
        if 'metadata' in kwargs:
            labeldict['metadata'] = kwargs.pop('metadata')

        if 'retry' not in kwargs:
            kwargs['retry'] = -1

        job = fts3.new_job(transfers, deletion, metadata=labeldict, **kwargs)
        job_id = fts3.submit(self.context, job)
        log.info('Submitted a new job with id %s' % job_id)
        return job_id
    
    def submit_request_context(self, transfers=None, deletion=None, **kwargs):
        """
        Submit a new job with a request context
        """
        # Label
        caller = inspect.stack()[1][3]
        labeldict = {'label': self.label, 'test': caller}

        if 'retry' not in kwargs:
            kwargs['retry'] = -1

        job = fts3.new_job(transfers, deletion, metadata=labeldict, **kwargs)
        request_context = fts3.Context(endpoint=self.endpoint, request_class=Request)
        job_id = fts3.submit(request_context, job)
        log.info('Submitted a new job with id %s' % job_id)
        return job_id
        
    def submit_delete_job(self, transfers=None, **kwargs):
        """
        Submit a new delete job
        """
        # Label
        caller = inspect.stack()[1][3]
        labeldict = {'label': self.label, 'test': caller}

        job = fts3.new_delete_job(transfers, metadata=labeldict, **kwargs)
        job_id = fts3.submit(self.context, job)
        log.info('Submitted a new delete job with id %s' % job_id)
        return job_id
        
    def submit_staging_job(self, transfers=None, **kwargs):
        """
        Submit a new staging job
        """
        # Label
        caller = inspect.stack()[1][3]
        labeldict = {'label': self.label, 'test': caller}
       
        job = fts3.new_staging_job(transfers, metadata=labeldict, **kwargs)
        job_id = fts3.submit(self.context, job)
        log.info('Submitted a new staging job with id %s' % job_id)
        return job_id

    def wait_for(self, job_id, state_in=['FAILED', 'FINISHED', 'CANCELED', 'FINISHEDDIRTY']):
        """
        Poll the remote server, and waits for job_id to finish
        """
        remaining = self.timeout
        log.info('Waiting for %s' % job_id)

        job = fts3.get_job_status(self.context, job_id, list_files=False)
        while job['job_state'] not in state_in:
            if remaining <= 0:
                raise Exception('Timeout expired while polling')
            time.sleep(self.poll_interval)
            remaining -= self.poll_interval
            log.info('Poll %s' % job_id)
            job = fts3.get_job_status(self.context, job_id, list_files=False)

        return job
    
    def wait_for_multiple(self, job_ids, state_in=['FAILED', 'FINISHED', 'CANCELED', 'FINISHEDDIRTY']):
        """
        Poll the remote server, and waits for job_ids to finish
        """
        remaining = self.timeout
        actives= len(job_ids)
        job_list = fts3.get_jobs_statuses(self.context, job_ids, list_files=False)
        while actives >  0:
            for job in job_list:
                if job['job_state'] not in state_in:
                    if remaining <= 0:
                        raise Exception('Timeout expired while polling')
                else:
                    pass
            actives=actives-1
            time.sleep(self.poll_interval)
            remaining -= self.poll_interval
            job_list = fts3.get_jobs_statuses(self.context, job_ids, list_files=False)

        return job_list

    def get(self, job_id, list_files=True):
        """
        Get files inside the job
        """
        log.info('Retrieving files for %s' % job_id)
        retries = 0
        while True:
            try:
                return fts3.get_job_status(self.context, job_id, list_files=list_files)
            except fts3.exceptions.TryAgain(e):
                retries += 1
                if retries > 3:
                    raise
                time.sleep(1)

    def get_retries(self, job_id, file_id):
        log.info('Retrieving retries for %s/%s' % (job_id, file_id))
        return json.loads(self.context.get('/jobs/%s/files/%d/retries' % (job_id, file_id)))

    def cancel(self, job_id, files_id=None):
        """
        Cancel job
        """
        log.info('Cancelling job %s' % job_id)
        return fts3.cancel(self.context, job_id, files_id)

    def cancel_all(self, vo):
        """
        Cancel all jobs for a given VO
        """
        log.info('Cancelling all jobs for %s' % vo)
        return fts3.cancel_all(self.context, vo)

    def ban_se(self, storage, **kwargs):
        """
        Ban a storage element
        """
        return fts3.ban_se(self.context, storage, **kwargs)

    def unban_se(self, storage):
        """
        Unban a storage element
        """
        return fts3.unban_se(self.context, storage)

    def ban_dn(self, user_dn, **kwargs):
        """
        Ban a user
        """
        return fts3.ban_dn(self.context, user_dn)

    def unban_dn(self, user_dn, **kwargs):
        """
        Unban a user
        """
        return fts3.unban_dn(self.context, user_dn)
