# FTS3 tests configuration
import os
import sys

# Client implementation
ClientImpl = os.environ.get('CLIENT_IMPL', 'lib.rest')

# FTS3 server
Fts3Host = os.environ.get('FTS3_HOST', 'fts3-devel.cern.ch')
Fts3Port = int(os.environ.get('FTS3_PORT', 8446))
Fts3MonPort = int(os.environ.get('FTS3_MON_PORT', 8449))
Fts3Endpoint = 'https://%s:%d' % (Fts3Host, Fts3Port)
Fts3MonEndpoint = 'https://%s:%d' % (Fts3Host, Fts3MonPort)

# Messaging
MsgBrokerHost = os.environ.get('BROKER', 'dashb-test-mb.cern.ch')
MsgBrokerPort = 61123
MsgComplete = '/topic/transfer.fts_monitoring_complete'
MsgStart = '/topic/transfer.fts_monitoring_start'
MsgState = '/topic/transfer.fts_monitoring_state'
MsgEnable = os.environ.get('FTS3_ENABLE_MSG', 'true').lower() == 'true'

# Certificate
UserCert = os.environ.get('X509_USER_CERT', '/etc/grid-security/hostcert.pem')
UserKey = os.environ.get('X509_USER_KEY', '/etc/grid-security/hostkey.pem')
Password = os.environ.get('X509_USER_KEY_PASSWORD', '')

# VO
Vo = os.environ.get('VO', 'dteam')

# Tests label (passed in the metadata)
TestLabel = 'fts3-tests'

# Polling interval in seconds
PollInterval = 5

# Job timeout in seconds
Timeout = 2700

# Checksum algorithm
Checksum = 'ADLER32'

# Logging level
import logging
try:
    debug = int(os.environ.get('DEBUG', 0))
except:
    debug = 1
if debug:
    logLevel = logging.DEBUG
else:
    logLevel = logging.INFO

# Let's make it nicer
logging.basicConfig(level = logLevel, format = '%(levelname)s %(message)s')

if sys.stdout.isatty():
    logging.addLevelName(logging.DEBUG, "\033[1;2m%-8s\033[1;m" % logging.getLevelName(logging.DEBUG))
    logging.addLevelName(logging.INFO, "\033[1;34m%-8s\033[1;m" % logging.getLevelName(logging.INFO))
    logging.addLevelName(logging.ERROR, "\033[1;31m%-8s\033[1;m" % logging.getLevelName(logging.ERROR))
    logging.addLevelName(logging.WARNING, "\033[1;33m%-8s\033[1;m" % logging.getLevelName(logging.WARNING))

# Stomp is way too verbose, so reduce unless debug
if not debug:
    logging.getLogger('stomp').setLevel(logging.ERROR)
else:
    logging.getLogger('stomp').setLevel(logging.WARN)
