import inspect
import json
import logging
import os
import subprocess
import tempfile
import time


log = logging.getLogger(__name__)


class ClientImpl(object):
    """
    Wrap the command line client
    """

    def _spawn(self, cmd_array, can_fail=False, stdin=None):
        """
        Spawn a command, wait for it and return the output
        """
        logging.debug("Spawning %s" % ' '.join(cmd_array))
        out = tempfile.NamedTemporaryFile()
        err = tempfile.NamedTemporaryFile()
        if stdin:
            proc = subprocess.Popen(cmd_array, stdout=out, stderr=err, stdin=subprocess.PIPE)
        else:
            proc = subprocess.Popen(cmd_array, stdout=out, stderr=err)
        if stdin:
            proc.stdin.write(stdin)
        rcode = proc.wait()
        out.seek(0)
        err.seek(0)
        if rcode != 0:
            if can_fail:
                logging.warning(out.read())
                logging.warning(err.read())
                return ''
            else:
                logging.error(out.read())
                logging.error(err.read())
                raise Exception("%s failed with exit code %d" % (cmd_array[0], rcode))
        return out.read().strip()

    def __init__(self, endpoint, poll_interval=2, timeout=600, label='fts3-tests'):
        
        self.endpoint = endpoint
        self.poll_interval = poll_interval
        self.timeout = timeout
        self.label = label

    def submit(self, transfers=None, deletion=None, **kwargs):
        """
        Submit a new job
        """
        if deletion:
            return self.submit_delete_job(deletion, **kwargs)

        # Adapt submission format
        for transfer in transfers:
            if 'checksum' in transfer:
                transfer['checksums'] = [transfer['checksum']]
                del transfer['checksum']

        # Label
        caller = inspect.stack()[1][3]
        labeldict = {'label': self.label, 'test': caller}
        if 'metadata' in kwargs:
            labeldict['metadata'] = kwargs.pop('metadata')
        label = json.dumps(labeldict)

        if 'retry' not in kwargs:
            kwargs['retry'] = -1

        submission = tempfile.NamedTemporaryFile(delete=False, suffix='.submission')
        submission.write(json.dumps({'Files': transfers}))
        submission.close()

        cmd_array = [
            'fts-transfer-submit',
            '-s', self.endpoint,
            '--job-metadata', label,
            '--retry', str(kwargs['retry']),
            '--json-submission', '-f', submission.name
        ]

        if kwargs.get('verify_checksum', False):
            cmd_array.append('--compare-checksums')
        if kwargs.get('overwrite', False):
            cmd_array.append('--overwrite')
        if kwargs.get('strict_copy', False):
            cmd_array.append('--strict-copy')
        if 'retry_delay' in kwargs:
            cmd_array += ['--retry-delay', str(kwargs['retry_delay'])]
        if 'bring_online' in kwargs:
            cmd_array += ['--bring-online', str(kwargs['bring_online'])]
        if kwargs.get('reuse', False):
            cmd_array.append('--reuse')
        if kwargs.get('multihop', False):
            cmd_array.append('--multi-hop')

        job_id = self._spawn(cmd_array)
        os.unlink(submission.name)
        return job_id

    def submit_request_context(self, transfers=None, deletion=None, **kwargs):
        """
        Submit a new job with a request context
        """
        return self.submit(transfers, deletion, **kwargs)

    def submit_delete_job(self, transfers=None, **kwargs):
        """
        Submit a new delete job
        """
        cmd_array = [
            'fts-transfer-delete',
            '-s', self.endpoint
        ] + transfers

        job_id = self._spawn(cmd_array)
        return job_id
        
    def submit_staging_job(self, transfers=None, **kwargs):
        """
        Submit a new staging job
        """
        rewritten_transfers = []
        for t in transfers:
            rewritten_transfers.append({
                'sources': [t],
                'destinations': [t]
            })
        return self.submit(rewritten_transfers, **kwargs)

    def get(self, job_id, list_files=True):
        """
        Get the job status
        """
        cmd_array = [
            'fts-transfer-status',
            '-s', self.endpoint,
            '--json', '-l', job_id, '--detailed'
        ]
        out = self._spawn(cmd_array)
        job = json.loads(out)['job'][0]

        # Need to reshape to fit what the tests are expecting
        job['job_state'] = job['status']
        del job['status']

        # Fake file_id too with the array index
        file_id = 0
        for f in job['files']:
            f['file_state'] = f['state']
            del f['state']
            f['source_surl'] = f['source']
            del f['source']
            f['dest_surl'] = f['destination']
            del f['destination']
            f['file_id'] = file_id
            file_id += 1
            if isinstance(f['retries'], int):
                f['retries'] = []

        return job

    def wait_for(self, job_id, state_in=['FAILED', 'FINISHED', 'CANCELED', 'FINISHEDDIRTY']):
        """
        Poll the remote server, and waits for job_id to finish
        """
        remaining = self.timeout
        log.info('Waiting for %s' % job_id)

        job = self.get(job_id)
        while job['job_state'] not in state_in:
            if remaining <= 0:
                raise Exception('Timeout expired while polling')
            time.sleep(self.poll_interval)
            remaining -= self.poll_interval
            log.info('Poll %s' % job_id)
            job = self.get(job_id)

        return job

    def get_retries(self, job_id, file_id):
        log.info('Retrieving retries for %s/%s' % (job_id, file_id))
        job = self.get(job_id, list_files=True)
        return job['files'][file_id]['retries']

    def cancel(self, job_id, files_id=None):
        """
        Cancel job
        """
        log.info('Cancelling job %s' % job_id)
        cmd_array = ['fts-transfer-cancel',
            '-s', self.endpoint, job_id
        ]
        self._spawn(cmd_array)

    def cancel_all(self, vo):
        """
        Cancel all jobs for a given VO
        """
        log.info('Cancelling all jobs for %s' % vo)
        cmd_array = ['fts-transfer-cancel',
            '-s', self.endpoint, '--cancel-all', '--voname', vo
        ]
        self._spawn(cmd_array, stdin='yes\n')

    def ban_se(self, storage, **kwargs):
        """
        Ban a storage element
        """
        if 'status' not in kwargs:
            cmd_array = [
                'fts-set-blacklist',
                '-s', self.endpoint, 'se', storage, 'ON'
            ]
        elif kwargs['status'].upper() in ('CANCEL', 'WAIT'):
            cmd_array = [
                'fts-set-blacklist',
                '-s', self.endpoint, 'se', storage, '--status=%s' % kwargs['status'].upper(), 'ON'
            ]
        else:
            raise Exception("Wrong status argument in fts-set-blacklist")
        self._spawn(cmd_array)

    def unban_se(self, storage):
        """
        Unban a storage element
        """
        cmd_array = ['fts-set-blacklist',
            '-s', self.endpoint, 'se', storage, 'OFF'
        ]
        self._spawn(cmd_array)

    def ban_dn(self, user_dn, **kwargs):
        """
        Ban a user
        """
        cmd_array = ['fts-set-blacklist',
            '-s', self.endpoint, 'dn', user_dn, 'ON'
        ]
        self._spawn(cmd_array)

    def unban_dn(self, user_dn, **kwargs):
        """
        Unban a user
        """
        cmd_array = ['fts-set-blacklist',
            '-s', self.endpoint, 'dn', user_dn, 'OFF'
        ]
        self._spawn(cmd_array)
