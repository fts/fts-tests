import json
import logging
import optparse
import os
import socket
import threading

import stomp

log = logging.getLogger(__name__)


def _resolve_brokers(alias, port):
    """
    To consume all the messages, we need to subscribe to all the hosts behind
    the DNS alias.
    :param alias: The DNS alias
    :param port: The TCP port
    :return: A list of tuples (resolved ip, port)
    """
    brokers = list()
    for family, _, _, _, addr in socket.getaddrinfo(alias, port, 0, 0, socket.IPPROTO_TCP):
        # Disable IPv6 until worker nodes can use it
        if family == socket.AF_INET:
            ip, port = addr
            brokers.append((ip, port))
    return brokers


class FTSSubConsumer(stomp.ConnectionListener):
    """
    Listener for each connection.
    """

    def __init__(self, parent, broker):
        """
        Constructor
        :param parent: The parent FTS consumer
        :param broker: (host, port)
        """
        super(stomp.ConnectionListener, self).__init__()
        self.parent = parent
        self.broker = broker

        self.connection = stomp.Connection([broker])
        self.connection.set_ssl(
            for_hosts=[broker],
            ca_certs=None,
            key_file=parent.key, cert_file=parent.cert,
            password=parent.get_password,
        )

    def start(self):
        """
        Start consuming
        """
        log.debug('Connecting to %s', str(self.broker))
        self.connection.set_listener('FTSSubConsumer', self)
        self.connection.start()
        self.connection.connect()

    def stop(self):
        """
        Stop consuming
        """
        log.debug('Disconnecting to %s', str(self.broker))
        self.connection.disconnect()
        self.connection.stop()
        self.connection.remove_listener('FTSSubConsumer')

    def on_connecting(self, host_and_port):
        log.debug('Messaging connected socket to %s' % str(host_and_port))

    def on_connected(self, headers, body):
        log.info('Messaging connected! %s' % headers)
        self.connection.subscribe(
            destination=self.parent.start_topic,
            id='fts-subscriber-start',
            ack='auto',
            receipt='subscribe-start'
        )
        self.connection.subscribe(
            destination=self.parent.complete_topic,
            id='fts-subscriber-end',
            ack='auto',
            receipt='subscribe-end'
        )
        self.connection.subscribe(
            destination=self.parent.state_topic,
            id='fts-tests-subscriber-state',
            ack='auto',
            receipt='subscribe-state'
        )

    def on_disconnected(self):
        log.critical('%s:%d disconnected!' % self.broker)

    def on_heartbeat_timeout(self):
        log.critical('%s:%d heartbeat timeout!' % self.broker)

    def on_message(self, headers, body):
        self.parent._on_message(headers, body)

    def on_receipt(self, headers, body):
        if 'receipt-id' in headers and headers['receipt-id'].startswith('subscribe-'):
            log.debug('Subscribed to %s' % headers['receipt-id'].split('-')[1])

    def on_error(self, headers, body):
        log.error('Messaging error: %s' % body)

    def on_send(self, frame):
        pass

    def on_heartbeat(self):
        pass


class FTSConsumer(object):
    """
    FTS messaging consumer. For doing something with the messages, it has to be inherited
    and the child re-implement the on_message_* methods.
    on_message_* methods calls will be serialized by this class.
    """

    def __init__(self, alias, port, cert, key, password,
                 start_topic='/topic/transfer.fts_monitoring_start',
                 complete_topic='/topic/transfer.fts_monitoring_complete',
                 state_topic='/topic/transfer.fts_monitoring_state'):
        """
        Constructor
        :param alias: Broker alias (i.e. dashb-mb.cern.ch)
        :param port: Broker port (i.e. 61123)
        :param cert: User X509 certificate
        :param key:  User private key
        :param password: User private key password
        :param start_topic: From where to consume the start messages
        :param complete_topic: From where to consume the completion messages
        :param state_topic: From where to consume the sate change messages
        """
        if cert:
            log.debug("Using user certificate: %s", cert)
        if key:
            log.debug("Using private key: %s", key)
        self.cert = cert
        self.key = key
        self.password = password
        self.start_topic = start_topic
        self.complete_topic = complete_topic
        self.state_topic = state_topic

        self.lock = threading.Lock()

        self.brokers = _resolve_brokers(alias, port)
        self.listeners = list()
        for broker in self.brokers:
            log.debug('Add broker %s:%d' % broker)
            self.listeners.append(FTSSubConsumer(self, broker))

    def get_password(self):
        """
        :return: The configured password
        """
        return self.password

    def start(self):
        """
        Start consuming
        """
        for listener in self.listeners:
            listener.start()

    def stop(self):
        """
        Stop consuming
        """
        for listener in self.listeners:
            listener.stop()

    def _on_message(self, headers, body):
        """
        Called by the child listeners
        """
        # Skip EOT
        eot = body.find('\x04')
        message = body[:eot]
        try:
            content = json.loads(message)
        except Exception(e):
            log.warn('Could not parse message from server!: %s' % str(e))
            log.warn(message)
            return

        try:
            self.lock.acquire()

            if headers['destination'] == self.start_topic:
                self.on_start_msg(content)
            elif headers['destination'] == self.complete_topic:
                self.on_complete_msg(content)
            elif headers['destination'] == self.state_topic:
                self.on_state_msg(content)
            else:
                log.warning("Unknown destination: %s" % headers['destination'])
        except Exception(e):
            log.error(str(e))
        finally:
            self.lock.release()

    def on_start_msg(self, msg):
        """
        Called when a start message is received
        """
        _, _, _, fileid, jobid = msg['transfer_id'].split('__')
        fileid = int(fileid)
        log.info('Start message for %s: %s %d (from %s)' % (msg['vo'], jobid, fileid, msg['endpnt']))

    def on_complete_msg(self, msg):
        """
        Called when a completion message is received
        """
        _, _, _, fileid, jobid = msg['tr_id'].split('__')
        fileid = int(fileid)
        log.info('Complete message for %s: %s %d (from %s)' % (msg['vo'], jobid, fileid, msg['endpnt']))

    def on_state_msg(self, msg):
        """
        Called when a state change message is received
        """
        log.info('State message for %(vo_name)s: %(job_id)s %(file_id)s (from %(endpnt)s)' % msg)


def _setup_logging(debug):
    logging.basicConfig(format='%(levelname)s %(message)s')
    if sys.stdout.isatty():
        logging.addLevelName(logging.DEBUG, "\033[1;2m%-8s\033[1;m" % logging.getLevelName(logging.DEBUG))
        logging.addLevelName(logging.INFO, "\033[1;34m%-8s\033[1;m" % logging.getLevelName(logging.INFO))
        logging.addLevelName(logging.ERROR, "\033[1;31m%-8s\033[1;m" % logging.getLevelName(logging.ERROR))
        logging.addLevelName(logging.WARNING, "\033[1;33m%-8s\033[1;m" % logging.getLevelName(logging.WARNING))

    if debug:
        logging.getLogger('stomp').setLevel(logging.INFO)
        log.setLevel(logging.DEBUG)
    else:
        logging.getLogger('stomp').setLevel(logging.WARN)
        log.setLevel(logging.INFO)

if __name__ == '__main__':
    """
    Can be used as a tool!
    """
    import sys
    import time

    parser = optparse.OptionParser()
    parser.add_option('-d', '--debug', action='store_true', default=False,
                      help='Increase verbosity')
    parser.add_option('-b', '--broker', default='dashb-mb.cern.ch',
                      help='Broker DNS alias')
    parser.add_option('-p', '--port', type=int, default=61123,
                      help='Broker port')
    parser.add_option('--cert', default=os.getenv('X509_USER_CERT', ''),
                      help='User certificate')
    parser.add_option('--key', default=os.getenv('X509_USER_KEY', ''),
                      help='User private key')
    opt, args = parser.parse_args()

    _setup_logging(opt.debug)

    consumer = FTSConsumer(alias=opt.broker, port=opt.port, cert=opt.cert, key=opt.key, password='')
    consumer.start()
    while True:
        time.sleep(5)
