"""
Helper methods to use mock urls handled by the gfal2 mock plugin
Using the mock plugin allows us to set the desired behaviour of the transfer,
testing effectively FTS3, and not the remote storage
"""
import random
import string
import urllib.parse as urlparse

HOSTS = [
    'test.cern.ch', 'test2.cern.ch',
    'somewhere.uk', 'aplace.es'
]


def random_string(_len):
    return ''.join(random.choice(string.ascii_lowercase) for _ in range(_len))


def random_host():
    return random.choice(HOSTS)


def random_path():
    return '/'.join(random_string(4) for _ in range(3))


def _parse_size(size):
    """
    Supports suffixes: b, K, M, G
    """
    if isinstance(size, int):
        return size
    val = int(size[:-1])
    suffix = size[-1].upper()
    if suffix == 'B':
        pass
    elif suffix == 'K':
        val *= 1024
    elif suffix == 'M':
        val *= (1024 ** 2)
    else:
        val *= (1024 ** 3)
    return val


def _parse_time(time):
    """
    Supports suffixes: s, m, h
    """
    if isinstance(time, int):
        return time
    val = int(time[:-1])
    suffix = time[-1].upper()
    if suffix == 'S':
        pass
    elif suffix == 'M':
        val *= 60
    else:
        val *= 3600
    return val


def generate_url(host=None, path=None, size=None, size_pre=None, size_post=None,
                 checksum=None, transfer_time=None, errno=None, transfer_errno=None,
                 staging_time=None, staging_errno=None, release_errno=None,
                 archiving_time=None, archiving_errno=None,
                 signal=None, signal_on_load=None, user_status=None):
    """
    :param host: Storage host name
    :param path: File path
    :param size: File size. None means 'does not exist'
    :param size_pre: File size when the url is the destination, before transferring. None means 'does not exist'
    :param size_post: File size when the url is the destination, after transferring. None means 'does not exist'
    :param checksum: File checksum (algorithm:value)
    :param transfer_time: Transfer time, only used from the destination url
    :param errno: Error code
    :param transfer_errno: Error code for transfers. Only makes sense in the destination url.
    :param staging_time: Staging time
    :param staging_errno: Errno raised on staging (bring online)
    :param release_errno: Errno raised on release
    :param archiving_time: Gfal2 archiving time
    :param archiving_errno: Errno raised on archive monitoring
    :param signal: Signal to raise during transfer execution
    :param signal_on_load: Signal to raise when the mock plugin is loaded
    :param user_status: File extended attribute "user.status"
    :return:
    """
    if not host:
        host = random_host()
    if not path:
        path = random_path()

    query = []
    if size:
        query.append('size=%d' % _parse_size(size))
    if size_pre:
        query.append('size_pre=%d' % _parse_size(size_pre))
    if size_post:
        query.append('size_post=%d' % _parse_size(size_post))
    if checksum:
        query.append('checksum=%s' % checksum)
    if transfer_time:
        query.append('time=%d' % _parse_time(transfer_time))
    if errno:
        query.append('errno=%d' % errno)
    if transfer_errno:
        query.append('transfer_errno=%d' % transfer_errno)
    if staging_time:
        query.append('staging_time=%d' % _parse_time(staging_time))
    if staging_errno:
        query.append('staging_errno=%d' % staging_errno)
    if release_errno:
        query.append('release_errno=%d' % release_errno)
    if archiving_time:
        query.append('archiving_time=%d' % _parse_time(archiving_time))
    if archiving_errno:
        query.append('archiving_errno=%d' % archiving_errno)
    if signal:
        query.append('signal=%d' % signal)
    if signal_on_load:
        query.append('MOCK_LOAD_TIME_SIGNAL%d' % signal_on_load)
    if user_status:
        query.append('user.status=%s' % user_status)

    return urlparse.urlunparse(('mock', host, path, None, '&'.join(query), None))


def generate_pair(source_host=None, dest_host=None, size='1M', transfer_time='2s', transfer_errno=None,
                  dest_exists=False, checksum=None, signal=None, staging_time=None, staging_errno=None,
                  archiving_time=None, archiving_errno=None, user_status=None,
                  signal_on_load=None):
    """
    Generate easily a pair of mock transfers that either succeeds, or
    fail with transfer_errno
    """
    src = generate_url(
        host=source_host, size=size, checksum=checksum, signal=signal,
        staging_time=staging_time, staging_errno=staging_errno,
        signal_on_load=signal_on_load
    )
    size_pre = None
    if dest_exists:
        size_pre = size
    dst = generate_url(
        host=dest_host, size_pre=size_pre, size_post=size, checksum=checksum,
        transfer_time=transfer_time, transfer_errno=transfer_errno,
        archiving_time=archiving_time, archiving_errno=archiving_errno,
        user_status=user_status
    )
    return src, dst
