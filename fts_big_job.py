#!/usr/bin/env python3
import logging
import unittest

from lib import TestCaseBase
from lib import mock_plugin_helper as mock


class TestBigJob(TestCaseBase):
    """
    Submit a job with a big number of files
    """

    def _test_parametrized(self, nfiles):
        base_url = mock.generate_url(path='/file.%04d')
        transfers = [{"sources" : [base_url % i + '?size=1000'], "destinations": [base_url % i + '.dst?size_post=1000&time=0']} for i in range(nfiles)]

        logging.info('Submitting a job with %d files' % nfiles)
        job_id = self.client.submit(transfers)
        job = self.client.wait_for(job_id)

        self.assertEqual('FINISHED', job['job_state'])
        job = self.client.get(job_id, list_files=True)
        self.assertEqual(nfiles, len(job['files']))

        # Check messaging
        if self.messaging:
            job_msgs = self.messaging.get_job_start(job_id, expected=nfiles)
            self.assertEqual(nfiles, len(job_msgs))

            job_msgs = self.messaging.get_job_completion(job_id, expected=nfiles)
            self.assertEqual(nfiles, len(job_msgs))
            for msg in job_msgs:
                self.assertEqual('Ok', msg['t_final_transfer_state'])

    @TestCaseBase.skip_for_short_run
    def _make_test(nfiles):
        """
        Generate dynamically the test method with nfiles
        """
        def inner(self):
            self._test_parametrized(nfiles)
        inner.__doc__ = "\nSubmit a job with %d files\n" % nfiles
        return inner

    test_100 = _make_test(100)
    test_500 = _make_test(500)


if __name__ == '__main__':
    unittest.main()
