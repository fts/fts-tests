#!/usr/bin/env python3
import unittest

from lib import TestCaseBase
from lib import mock_plugin_helper as mock


class TestOverwrite(TestCaseBase):
    """
    Test overwrite jobs
    """

    def test_overwrite(self):
        """
        Enable overwrite and submit a transfer with a file that does exist.
        It must succeed.
        """
        src, dst = mock.generate_pair(dest_exists=True)
        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            overwrite=True
        )
        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msgs))

            self.assertEqual('Ok', job_msgs[0]['t_final_transfer_state'])

    def test_no_overwrite(self):
        """
        Do NOT enable overwrite and submit a transfer with a file that does exist.
        It must fail.
        """
        src, dst = mock.generate_pair(dest_exists=True)
        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            overwrite=False
        )
        job = self.client.wait_for(job_id)
        self.assertEqual('FAILED', job['job_state'])

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msgs))

            self.assertEqual('Error', job_msgs[0]['t_final_transfer_state'])
            self.assertEqual('DESTINATION', job_msgs[0]['tr_error_scope'])

if __name__ == '__main__':
    unittest.main()
