#!/usr/bin/env python3
import errno
import unittest

from lib import TestCaseBase
from lib import mock_plugin_helper as mock


class TestMultiple(TestCaseBase):
    """
    Test job with multiple transfers and reuse disabled
    """

    NPAIRS = 5

    def test_multiple(self):
        """
        Transfer multiple files
        """
        transfers = []
        for i in range(self.NPAIRS):
            src, dst = mock.generate_pair()
            transfers.append({'sources': [src], 'destinations': [dst]})

        job_id = self.client.submit(transfers)
        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        for f in job['files']:
            self.assertEqual('FINISHED', f['file_state'])

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id, expected=self.NPAIRS)
            self.assertEqual(self.NPAIRS, len(job_msgs))
            for msg in job_msgs:
                self.assertEqual('Ok', msg['t_final_transfer_state'])

    def test_multiple_dirty(self):
        """
        Transfer multiple files, one does not exist
        """
        transfers = []
        for i in range(self.NPAIRS):
            src, dst = mock.generate_pair()
            transfers.append({'sources': [src], 'destinations': [dst]})

        # Replace last one with a enoent
        bad_src = mock.generate_url(errno=errno.ENOENT)
        transfers[-1]['sources'] = [bad_src]

        job_id = self.client.submit(transfers)
        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHEDDIRTY', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        for f in job['files']:
            if f['source_surl'] == bad_src:
                self.assertEqual('FAILED', f['file_state'])
            else:
                self.assertEqual('FINISHED', f['file_state'])

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id, expected=self.NPAIRS)
            self.assertEqual(self.NPAIRS, len(job_msgs))
            for msg in job_msgs:
                if bad_src == msg['src_url']:
                    self.assertEqual('Error', msg['t_final_transfer_state'])
                else:
                    self.assertEqual('Ok', msg['t_final_transfer_state'])

    def test_multiple_dirty2(self):
        """
        Same as before, but let the first fail. Error message should not
        affect other transfers
        """
        transfers = []
        for i in range(self.NPAIRS):
            src, dst = mock.generate_pair()
            transfers.append({'sources': [src], 'destinations': [dst]})

        # Replace last one with a enoent
        bad_src = mock.generate_url()
        transfers[0]['sources'] = [bad_src]

        job_id = self.client.submit(transfers)
        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHEDDIRTY', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        for f in job['files']:
            if f['source_surl'] == bad_src:
                self.assertEqual('FAILED', f['file_state'])
            else:
                self.assertEqual('FINISHED', f['file_state'])

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id, expected=self.NPAIRS)
            self.assertEqual(self.NPAIRS, len(job_msgs))
            for msg in job_msgs:
                if bad_src == msg['src_url']:
                    self.assertEqual('Error', msg['t_final_transfer_state'])
                else:
                    self.assertEqual('Ok', msg['t_final_transfer_state'])


if __name__ == '__main__':
    unittest.main()
