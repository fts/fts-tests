#!/usr/bin/env python3
import logging
import unittest
import string
import random
import uuid

from lib import TestCaseBase
from lib import mock_plugin_helper as mock
from fts3.rest.client.easy import JobIdGenerator, exceptions

N = 32
UUID = str(uuid.uuid4())
class TestJobIdGenerator(TestCaseBase):
    """
    Test deterministic job id generator.
    """

    def test_deterministic_successful(self):
        """
        Simple job with source and destination pair by using the deterministic job id generator
        which uses uuid5 + vo_name + sid given by the client.
        """
        src, dst = mock.generate_pair()
        
        job_id = self.client.submit([{'sources': [src], 'destinations': [dst]}], 
                                    id_generator=JobIdGenerator.deterministic, 
                                    sid=''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) 
                                                for _ in range(N)))

        # Validate polling
        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        if self.messaging:
            # Validate start message
            job_msg = self.messaging.get_job_start(job_id)
            self.assertEqual(1, len(job_msg))
            
            # Validate completion message
            job_msg = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msg))
            job_msg = job_msg[0]
            self.assertEqual('UNKNOWN', job_msg['job_state'])
            self.assertEqual(src, job_msg['src_url'])
            self.assertEqual(dst, job_msg['dst_url'])
            self.assertEqual('Ok', job_msg['t_final_transfer_state'])

        logging.info('Finished with %s' % job['job_state'])

    @TestCaseBase.skip_for_short_run
    def test_duplicated_sid(self):
        """
        Simple job with source and destination pair by using the deterministic job id generator
        which uses uuid5 + vo_name + sid given by the client in this case duplicated.
        """
        src, dst = mock.generate_pair()
        job_id1 = self.client.submit([{'sources': [src], 'destinations': [dst]}], 
                                    id_generator=JobIdGenerator.deterministic, 
                                    sid=UUID)

        try:
            job_id2 = self.client.submit([{'sources': [src], 'destinations': [dst]}], 
                                    id_generator=JobIdGenerator.deterministic, 
                                    sid=UUID)
            self.fail("Submission should have failed!")
        except exceptions.ClientError:
            # There should have been a duplicated error
            pass

if __name__ == '__main__':
    unittest.main()
