#!/usr/bin/env python3
import unittest

from lib import TestCaseBase
from lib import mock_plugin_helper as mock


class TestDestinationFileReport(TestCaseBase):
    """
    Test destination file report
    """

    def test_destination_file_online_report(self):
        """
        Archive transfer with destination file report enabled and overwrite disabled.
        Transfer fails with destination file exists,
        but file_metadata field is populated with a report on the destination file.
        File on disk but not on tape.
        """
        src = mock.generate_url(size=100)
        
        dst = mock.generate_url(checksum='08h32v41', user_status='ONLINE',
            size=100, size_pre=100, size_post=100)

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            overwrite=False, archive_timeout=3600, dst_file_report=True
        )
        job = self.client.wait_for(job_id)
        self.assertEqual('FAILED', job['job_state'])

        job = self.client.get(job_id)
        self.assertIn('dst_file', job['files'][0]['file_metadata'])

        file_report = job['files'][0]['file_metadata']['dst_file']
        self.assertEqual(100, file_report['file_size'])
        self.assertEqual('08h32v41', file_report['checksum_value'])
        self.assertEqual('ADLER32', file_report['checksum_type'])
        self.assertEqual(False, file_report['file_on_tape'])
        self.assertEqual(True, file_report['file_on_disk'])
        
        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msgs))
            self.assertEqual('Error', job_msgs[0]['t_final_transfer_state'])
            self.assertEqual('Destination file exists and overwrite is not enabled', job_msgs[0]['t__error_message'])
            self.assertIn('dst_file', job_msgs[0]['file_metadata'])

            file_report = job_msgs[0]['file_metadata']['dst_file']
            self.assertEqual(100, file_report['file_size'])
            self.assertEqual('08h32v41', file_report['checksum_value'])
            self.assertEqual('ADLER32', file_report['checksum_type'])
            self.assertEqual(False, file_report['file_on_tape'])
            self.assertEqual(True, file_report['file_on_disk'])

    def test_destination_file_online_and_nearline_report(self):
        """
        Archive transfer with destination file report enabled and overwrite disabled.
        Transfer fails with destination file exists,
        but file_metadata field is populated with a report on the destination file.
        File on disk and on tape.
        """
        src = mock.generate_url(size=100)

        dst = mock.generate_url(checksum='08h32v41', user_status='ONLINE_AND_NEARLINE',
            size=100, size_pre=100, size_post=100)

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            overwrite=False, archive_timeout=3600, dst_file_report=True
        )
        job = self.client.wait_for(job_id)
        self.assertEqual('FAILED', job['job_state'])

        job = self.client.get(job_id)
        self.assertIn('dst_file', job['files'][0]['file_metadata'])

        file_report = job['files'][0]['file_metadata']['dst_file']
        self.assertEqual(100, file_report['file_size'])
        self.assertEqual('08h32v41', file_report['checksum_value'])
        self.assertEqual('ADLER32', file_report['checksum_type'])
        self.assertEqual(True, file_report['file_on_tape'])
        self.assertEqual(True, file_report['file_on_disk'])

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msgs))
            self.assertEqual('Error', job_msgs[0]['t_final_transfer_state'])
            self.assertEqual('Destination file exists and overwrite is not enabled', job_msgs[0]['t__error_message'])
            self.assertIn('dst_file', job_msgs[0]['file_metadata'])

            file_report =  job_msgs[0]['file_metadata']['dst_file']
            self.assertEqual(100, file_report['file_size'])
            self.assertEqual('08h32v41', file_report['checksum_value'])
            self.assertEqual('ADLER32', file_report['checksum_type'])
            self.assertEqual(True, file_report['file_on_tape'])
            self.assertEqual(True, file_report['file_on_disk'])

    def test_no_destination_file_report_flag(self):
        """
        Archive transfer without destination file report enabled.
        Transfer fails with destination file exists,
        and destination file report is not added to file_metadata.
        """
        src = mock.generate_url(size=100)

        dst = mock.generate_url(checksum='08h32v41', user_status='ONLINE',
            size=100, size_pre=100, size_post=100)

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            overwrite=False, archive_timeout=3600,
        )
        job = self.client.wait_for(job_id)
        self.assertEqual('FAILED', job['job_state'])

        job = self.client.get(job_id)
        self.assertEqual(None, job['files'][0]['file_metadata'])

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msgs))
            self.assertEqual('Error', job_msgs[0]['t_final_transfer_state'])
            self.assertEqual('Destination file exists and overwrite is not enabled', job_msgs[0]['t__error_message'])
            self.assertEqual('', job_msgs[0]['file_metadata'])

    def test_destination_file_report_with_overwrite(self):
        """
        Transfer file with destination file report enabled but overwrite flag is enabled.
        Transfer finishes with success and destination file report is not added to file_metadata.
        """
        src = mock.generate_url(size=100)

        dst = mock.generate_url(checksum='08h32v41', user_status='ONLINE',
            size=100, size_pre=100, size_post=100)

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            overwrite=True, dst_file_report=True
        )
        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        job = self.client.get(job_id)
        self.assertEqual(None, job['files'][0]['file_metadata'])

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msgs))
            self.assertEqual('Ok', job_msgs[0]['t_final_transfer_state'])
            self.assertEqual('', job_msgs[0]['file_metadata'])

if __name__ == '__main__':
    unittest.main()
