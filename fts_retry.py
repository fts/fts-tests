#!/usr/bin/env python3
import errno
import unittest

from lib import TestCaseBase
from lib import mock_plugin_helper as mock


class TestRetry(TestCaseBase):
    """
    Test retrials
    """

    @TestCaseBase.skip_for_short_run
    def test_retry_not_recoverable(self):
        """
        Submit a transfer that will fail with source does not exist. It must NOT be retried
        even if set.
        """
        src = mock.generate_url(errno=errno.ENOENT)
        dst = mock.generate_url()

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            retry=1, retry_delay=2
        )

        self.client.wait_for(job_id)
        job = self.client.get(job_id, list_files=True)

        self.assertEqual('FAILED', job['job_state'])
        self.assertEqual('FAILED', job['files'][0]['file_state'])
        retries = self.client.get_retries(job_id, job['files'][0]['file_id'])
        self.assertEqual(0, len(retries))

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msgs))
            self.assertEqual('Error', job_msgs[0]['t_final_transfer_state'])
            self.assertEqual(False, int(job_msgs[0]['is_recoverable']))
            self.assertEqual(1, int(job_msgs[0]['retry_max']))
            self.assertEqual(0, int(job_msgs[0]['retry']))

            # State changes
            file_id = int(job_msgs[0]['tr_id'].split('__')[3])
            self.assertStateTransitions(job_id, file_id, 'SUBMITTED', 'READY', 'ACTIVE', 'FAILED')

    @TestCaseBase.skip_for_short_run
    def test_retry_0(self):
        """
        Submit a transfer that will fail with an error that should be retried,
        but ask NOT TO retry, so there should be no retries
        """
        src, dst = mock.generate_pair(transfer_errno=errno.ECONNREFUSED)

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            retry=-1, retry_delay=2
        )

        self.client.wait_for(job_id)
        job = self.client.get(job_id, list_files=True)

        self.assertEqual('FAILED', job['job_state'])
        self.assertEqual('FAILED', job['files'][0]['file_state'])
        retries = self.client.get_retries(job_id, job['files'][0]['file_id'])
        self.assertEqual(0, len(retries))

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msgs))
            self.assertEqual('Error', job_msgs[0]['t_final_transfer_state'])
            self.assertEqual(True, int(job_msgs[0]['is_recoverable']))
            self.assertEqual(0, int(job_msgs[0]['retry_max']))
            self.assertEqual(0, int(job_msgs[0]['retry']))

            # State changes
            file_id = int(job_msgs[0]['tr_id'].split('__')[3])
            self.assertStateTransitions(job_id, file_id, 'SUBMITTED', 'READY', 'ACTIVE', 'FAILED')

    @TestCaseBase.skip_for_short_run
    def test_retry(self):
        """
        Submit a transfer that will fail with an error that should be retried.
        """
        src, dst = mock.generate_pair(transfer_errno=errno.ECONNREFUSED)

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            retry=1, retry_delay=2
        )

        self.client.wait_for(job_id)
        job = self.client.get(job_id, list_files=True)

        self.assertEqual('FAILED', job['job_state'])
        self.assertEqual('FAILED', job['files'][0]['file_state'])
        retries = self.client.get_retries(job_id, job['files'][0]['file_id'])
        self.assertEqual(1, len(retries))

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id)
            self.assertEqual(2, len(job_msgs))
            self.assertEqual('Error', job_msgs[0]['t_final_transfer_state'])
            self.assertEqual('Error', job_msgs[1]['t_final_transfer_state'])
            self.assertEqual(True, int(job_msgs[0]['is_recoverable']))
            self.assertEqual(True, int(job_msgs[1]['is_recoverable']))
            self.assertEqual(1, int(job_msgs[0]['retry_max']))
            self.assertEqual(0, int(job_msgs[0]['retry']))
            self.assertEqual(1, int(job_msgs[1]['retry']))

            # State changes
            file_id = int(job_msgs[0]['tr_id'].split('__')[3])
            self.assertStateTransitions(job_id, file_id, 'SUBMITTED', 'READY', 'ACTIVE', 'SUBMITTED', 'FAILED')

    @TestCaseBase.skip_for_short_run
    def test_retry_checksum_dest(self):
        """
        Checksum at destination validation should be retried.
        """
        src = mock.generate_url(size='1M', checksum='1a2b3c')
        dst = mock.generate_url(size_post='1M', checksum='000000')

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            retry=1, retry_delay=2, verify_checksum = True
        )

        self.client.wait_for(job_id)
        job = self.client.get(job_id, list_files=True)

        self.assertEqual('FAILED', job['job_state'])
        self.assertEqual('FAILED', job['files'][0]['file_state'])
        retries = self.client.get_retries(job_id, job['files'][0]['file_id'])
        self.assertEqual(1, len(retries))

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id)
            self.assertEqual(2, len(job_msgs))
            self.assertEqual('Error', job_msgs[0]['t_final_transfer_state'])
            self.assertEqual('Error', job_msgs[1]['t_final_transfer_state'])
            self.assertEqual(True, int(job_msgs[0]['is_recoverable']))
            self.assertEqual(True, int(job_msgs[1]['is_recoverable']))
            self.assertEqual(1, int(job_msgs[0]['retry_max']))
            self.assertEqual(0, int(job_msgs[0]['retry']))
            self.assertEqual(1, int(job_msgs[1]['retry']))

            # State changes
            file_id = int(job_msgs[0]['tr_id'].split('__')[3])
            self.assertStateTransitions(job_id, file_id, 'SUBMITTED', 'READY', 'ACTIVE', 'SUBMITTED', 'FAILED')

    @TestCaseBase.skip_for_short_run
    def test_retry_staging(self):
        """
        Submit a transfer for staging that will fail during the transfer, with an error that
        should be retried. Retry counter should go up.
        Regression for FTS-552
        """
        src, dst = mock.generate_pair(transfer_errno=errno.ETIMEDOUT)

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            retry=1, retry_delay=2, bring_online=120
        )

        self.client.wait_for(job_id)
        job = self.client.get(job_id, list_files=True)

        self.assertEqual('FAILED', job['job_state'])
        self.assertEqual('FAILED', job['files'][0]['file_state'])
        retries = self.client.get_retries(job_id, job['files'][0]['file_id'])
        self.assertEqual(1, len(retries))

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id)
            self.assertEqual(2, len(job_msgs))
            self.assertEqual('Error', job_msgs[0]['t_final_transfer_state'])
            self.assertEqual('Error', job_msgs[1]['t_final_transfer_state'])
            self.assertEqual(True, int(job_msgs[0]['is_recoverable']))
            self.assertEqual(True, int(job_msgs[1]['is_recoverable']))
            self.assertEqual(1, int(job_msgs[0]['retry_max']))
            self.assertEqual(0, int(job_msgs[0]['retry']))
            self.assertEqual(1, int(job_msgs[1]['retry']))


if __name__ == '__main__':
    unittest.main()
