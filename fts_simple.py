#!/usr/bin/env python3
import errno
import logging
import unittest

from lib import TestCaseBase
from lib import mock_plugin_helper as mock


class TestSimpleSubmission(TestCaseBase):
    """
    Test simple submissions. Just a source and a destination.
    """

    def test_submit_successful(self):
        """
        Simplest job with source and destination pair, all goes good, nothing special
        """
        src, dst = mock.generate_pair()

        job_id = self.client.submit([
            {
                'sources': [src],
                'destinations': [dst],
                'metadata': {
                    'myownid': '126364',
                    'whatever': False
                }
            }
        ])

        # Validate polling
        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        # Validate messages
        if self.messaging:
            start_msgs = self.messaging.get_job_start(job_id)
            end_msgs = self.messaging.get_job_completion(job_id)
            self.assertTrue(len(start_msgs) and len(end_msgs))

            # Start
            self.assertEqual('126364', start_msgs[0]['file_metadata']['myownid'])
            self.assertIn(bool(start_msgs[0]['file_metadata']['whatever']), [False, 'False', 'false'])

            # Completion
            msg = end_msgs[0]
            self.assertEqual('UNKNOWN', msg['job_state'])
            self.assertEqual(src, msg['src_url'])
            self.assertEqual(dst, msg['dst_url'])
            self.assertEqual('Ok', msg['t_final_transfer_state'])

            self.assertEqual('126364', msg['file_metadata']['myownid'])
            self.assertEqual(False, msg['file_metadata']['whatever'])

            logging.info('Finished with %s' % job['job_state'])

            # Validate state changes
            file_id = int(msg['tr_id'].split('__')[3])
            self.assertStateTransitions(job_id, file_id, 'SUBMITTED', 'READY', 'ACTIVE', 'FINISHED')

    def test_submit_source_enoent(self):
        """
        Source does not exist
        """
        src = mock.generate_url(errno=errno.ENOENT)
        dst = mock.generate_url(size_pre=-1, size_post='1M', transfer_time='2s')

        job_id = self.client.submit([
            {
                'sources': [src],
                'destinations': [dst],
                'metadata': {
                    'value': 42
                }
            }
        ])

        # Validate polling
        job = self.client.wait_for(job_id)
        self.assertEqual('FAILED', job['job_state'])

        # Validate messages
        if self.messaging:
            start_msgs = self.messaging.get_job_start(job_id)
            end_msgs = self.messaging.get_job_completion(job_id)
            self.assertTrue(len(start_msgs) and len(end_msgs))

            # Start
            self.assertEqual(42, int(start_msgs[0]['file_metadata']['value']))

            # Validate completion message
            msg = end_msgs[0]
            self.assertEqual('UNKNOWN', msg['job_state'])
            self.assertEqual(src, msg['src_url'])
            self.assertEqual(dst, msg['dst_url'])
            self.assertEqual('Error', msg['t_final_transfer_state'])
            self.assertEqual('SOURCE', msg['tr_error_scope'])
            self.assertEqual('NO_SUCH_FILE_OR_DIRECTORY', msg['tr_error_category'])
            self.assertEqual(False, int(msg['is_recoverable']))

            self.assertEqual(42, int(msg['file_metadata']['value']))

            logging.info('Finished with %s' % job['job_state'])

            # Validate state changes
            file_id = int(msg['tr_id'].split('__')[3])
            self.assertStateTransitions(job_id, file_id, 'SUBMITTED', 'ACTIVE', 'READY', 'FAILED')


if __name__ == '__main__':
    unittest.main()
