#!/bin/bash
set -e

function create_egi_trustanchors_repo {
  echo '[EGI-trustanchors]
name=EGI-trustanchors
baseurl=http://repository.egi.eu/sw/production/cas/1/current/
enabled=1
gpgcheck=1
gpgkey=http://repository.egi.eu/sw/production/cas/1/GPG-KEY-EUGridPMA-RPM-3' > /etc/yum.repos.d/EGI-trustanchors.repo
}

function create_vomses_file {
  mkdir -p /etc/vomses
  echo '"dteam" "voms-dteam-auth.cern.ch" "443" "/DC=ch/DC=cern/OU=computers/CN=dteam-auth.cern.ch" "dteam" "24"' > /etc/vomses/dteam-voms-dteam-auth.cern.ch
}

function create_vomsdir_file {
  mkdir -p /etc/grid-security/vomsdir/dteam
  echo "/DC=ch/DC=cern/OU=computers/CN=dteam-auth.cern.ch" > /etc/grid-security/vomsdir/dteam/voms-dteam-auth.cern.ch.lsc
  echo "/DC=ch/DC=cern/CN=CERN Grid Certification Authority" >> /etc/grid-security/vomsdir/dteam/voms-dteam-auth.cern.ch.lsc
}

create_egi_trustanchors_repo
create_vomses_file
create_vomsdir_file
