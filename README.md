FTS Tests
=========

This repository hosts the new FTS3 test collection.

In order to run these tests, you will need to install `stomp.py`, `pytest` and the `fts-rest-client` Python3 API.

The helper script `run-helper.sh` is provided, which will create a virtual environment
and take care of installing the dependencies.

#### Requirements

A valid X509 proxy with `dteam` VOMS data is needed.

In case you want to also test the ActiveMQ messaging, point the environment variables
`X509_USER_CERT` and `X509_USER_KEY` to a user certificate and private key (not protected by password!) pair
in order to let `stomp.py` connect to the message broker.

#### Environment variables

There are certain environment variables which control the configuration
and behavior of the test suite.

```
# Behavior
SHORT_RUN - Enable only a partial run of the test suite by skipping bringonline and other lengthy tests (default: 0)
NO_BRINGONLINE - Skip the "fts_bringonline.py" test case when set to true (default: None)
NO_SIGNALS - Skip the "fts_url_copy_signals.py" test case when set to true (default: None)
DEBUG - Enable or disable debug printing (default: 0)

# Application
FTS_MULTIHOP_UNUSED_STATE - File state of unused transfers in a multihop job (default: "NOT_USED")
CLIENT_IMPL - The FTS3 Python Client module name (default: "lib.rest")

# FTS3 Instance
FTS3_HOST - The FTS3 Instance to connect to (default: "fts3-devel.cern.ch")
FTS3_PORT - The FTS3 REST API port (default: 8446)
FTS3_MON_PORT - The FTS3 Web Monitoring port (default: 8449)

# Messaging
BROKER - The ActiveMQ broker to connect to (default: "dashb-test-mb.cern.ch")
FTS3_ENABLE_MSG - Enable or disable ActiveMQ messaging tests (default: True)
VO - VO used for filtering when listening to the ActiveMQ messages (default: "dteam")

# Certificate
X509_USER_CERT - The user certificate location (default: "/etc/grid-security/hostcert.pem")
X509_USER_KEY - The user's private key location (default: "/etc/grid-security/hostkey.pem")
X509_USER_KEY_PASSWORD - The user's private key password
```
