#!/usr/bin/env python3
import errno
import unittest

from lib import TestCaseBase
from lib import mock_plugin_helper as mock


class TestMultipleJobs(TestCaseBase):
    """
    Test multiple jobs with multiple transfers and reuse disabled
    """

    NPAIRS = 3
    NJOBS  = 3

    def test_multiple_jobs(self):
        """
        Transfer multiple jobs and files
        """
        job_ids = []
        for i in range(self.NJOBS):
            transfers = []
            for i in range(self.NPAIRS):
                src, dst = mock.generate_pair()
                transfers.append({'sources': [src], 'destinations': [dst]})
            job_id = self.client.submit(transfers)
            job_ids.append(job_id)
        job_ids = self.client.wait_for_multiple(job_ids)
        for job in job_ids:
            self.assertEqual('FINISHED', job['job_state'])

            job = self.client.get(job_id, list_files=True)
            for f in job['files']:
                self.assertEqual('FINISHED', f['file_state'])

if __name__ == '__main__':
    unittest.main()
