#!/usr/bin/env python3
import unittest

from lib import TestCaseBase, config
from lib import mock_plugin_helper as mock


class TestPriority(TestCaseBase):
    """
    Test priority queue.
    """

    NLOWER = 50

    @TestCaseBase.skip_for_short_run
    def test_submit_higher_priority(self):
        """
        Submit a bunch of jobs, then another one with higher priority.
        Should enter directly.
        """
        if config.ClientImpl == 'lib.cli':
            return

        job_ids = []
        for i in range(self.NLOWER):
            src, dst = mock.generate_pair(source_host='source.cern.ch', dest_host='somewhere.uk', transfer_time='10s')
            job_ids.append(self.client.submit(
                [{'sources': [src], 'destinations': [dst]}],
                priority=2
            ))

        src, dst = mock.generate_pair(source_host='source.cern.ch', dest_host='somewhere.uk', transfer_time='1s')
        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            priority=4
        )

        self.client.wait_for(job_id)

        # There should be some yet in the queue
        not_executed = 0
        for job_id in job_ids:
            job = self.client.get(job_id)
            if job['job_state'] in ('SUBMITTED', 'READY', 'ACTIVE'):
                not_executed += 1

        self.assertGreater(not_executed, 0)

        # Cancel remaining
        for job_id in job_ids:
            self.client.cancel(job_id)


if __name__ == '__main__':
    unittest.main()
