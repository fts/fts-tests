#!/usr/bin/env python3
import errno
import unittest

from lib import TestCaseBase
from lib import mock_plugin_helper as mock

from fts3.rest.client.exceptions import ClientError

class TestMultipleSources(TestCaseBase):
    """
    Test job with multiple sources
    """

    NSOURCES = 3

    def test_multiple_sources_and_reuse(self):
        """
        Submitting multiple replicas with reuse enabled must fail,
        since those are two incompatible modes
        """
        sources = []
        for i in range(self.NSOURCES):
            sources.append(mock.generate_url(size='1M'))
        dst = mock.generate_url(transfer_time='1s')

        try:
            self.client.submit(
                [{'sources': sources, 'destinations': [dst]}],
                reuse=True
            )
            self.fail('Should have failed')
        except (ClientError, Exception):
            pass
        except Exception(e):
            self.fail('Unexpected exception: %s' % str(e))

    def test_multiple_sources_all_enoent(self):
        """
        Three source replicas are given, but none of them exists,
        so the job final status must be failed
        """
        sources = []
        for i in range(self.NSOURCES):
            sources.append(mock.generate_url(errno=errno.ENOENT))
        dst = mock.generate_url(transfer_time='1s', size_post='1M')

        job_id = self.client.submit(
            [{
                'sources': sources,
                'destinations': [dst],
                'selection_strategy': 'orderly'
            }]
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FAILED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        for f in job['files']:
            self.assertEqual('FAILED', f['file_state'])

        # Three messages should have been set, only one terminal!
        if self.messaging:
            job_msg = self.messaging.get_job_completion(job_id, expected=3)
            self.assertEqual(3, len(job_msg))
            msg_states_count = dict()

            for msg in job_msg:
                self.assertEqual('Error', msg['t_final_transfer_state'])
                self.assertEqual('NO_SUCH_FILE_OR_DIRECTORY', msg['tr_error_category'])
                msg_state = msg['job_state']
                msg_states_count[msg_state] = msg_states_count.get(msg_state, 0) + 1

            self.assertEqual(2, msg_states_count['ACTIVE'])
            self.assertEqual(1, msg_states_count['FAILED'])

    def test_multiple_one_exists(self):
        """
        Three source replicas are given, and only one exists.
        The transfer must succeed.
        """
        sources = []
        for i in range(self.NSOURCES):
            sources.append(mock.generate_url(errno=errno.ENOENT))
        dst = mock.generate_url(transfer_time='1s', size_post='1M')

        # Replace last source with a file that exists
        ok_src = mock.generate_url(size='1M')
        sources[-1] = ok_src

        job_id = self.client.submit(
            [{
                'sources': sources,
                'destinations': [dst],
                'selection_strategy': 'orderly'
            }]
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        nFailed = 0
        nFinished = 0
        for f in job['files']:
            if f['file_state'] == 'FINISHED':
                nFinished += 1
            elif f['file_state'] == 'FAILED':
                nFailed += 1

        self.assertEqual(nFinished, 1)
        self.assertLessEqual(nFailed, self.NSOURCES - 1)

        # Max three messages should have been set, only one terminal!
        if self.messaging:
            job_msg = self.messaging.get_job_completion(job_id)
            self.assertLessEqual(len(job_msg), 3)
            msg_states_count = dict()

            for msg in job_msg:
                msg_state = msg['job_state']
                if msg_state == 'ACTIVE':
                    self.assertEqual('Error', msg['t_final_transfer_state'])
                    self.assertEqual('NO_SUCH_FILE_OR_DIRECTORY', msg['tr_error_category'])
                else:
                    self.assertEqual('Ok', msg['t_final_transfer_state'])
                msg_states_count[msg_state] = msg_states_count.get(msg_state, 0) + 1

            self.assertLessEqual(msg_states_count['ACTIVE'], 2)
            self.assertEqual(1, msg_states_count['FINISHED'])

    def test_first_ok(self):
        """
        Regression for FTS-350
        """
        sources = []
        for i in range(self.NSOURCES):
            sources.append(mock.generate_url(size='1M'))
        dst = mock.generate_url(transfer_time='1s', size_post='1M')

        job_id = self.client.submit(
            [{
                'sources': sources,
                'destinations': [dst],
                'selection_strategy': 'orderly'
            }]
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        for f in job['files']:
            if f['source_surl'] == sources[0]:
                self.assertEqual('FINISHED', f['file_state'])
            else:
                self.assertEqual('NOT_USED', f['file_state'])

    def test_auto_all_fail(self):
        """
        Regression for FTS-872
        """
        sources = []
        for i in range(self.NSOURCES):
            sources.append(mock.generate_url(errno=errno.ENOENT))
        dst = mock.generate_url(transfer_time='1s', size_post='1M')

        job_id = self.client.submit(
            [{
                'sources': sources,
                'destinations': [dst],
                'selection_strategy': 'auto'
            }]
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FAILED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        for f in job['files']:
            self.assertEqual('FAILED', f['file_state'])

        # Three messages should have been set, only one terminal!
        if self.messaging:
            job_msg = self.messaging.get_job_completion(job_id, expected=3)
            self.assertEqual(3, len(job_msg))
            msg_states_count = dict()

            for msg in job_msg:
                self.assertEqual('Error', msg['t_final_transfer_state'])
                self.assertEqual('NO_SUCH_FILE_OR_DIRECTORY', msg['tr_error_category'])
                msg_state = msg['job_state']
                msg_states_count[msg_state] = msg_states_count.get(msg_state, 0) + 1

            self.assertEqual(2, msg_states_count['ACTIVE'])
            self.assertEqual(1, msg_states_count['FAILED'])

    def test_one_cancels(self):
        """
        Give two replicas. The first one will fail with ECANCELED
        Regression for FTS-1073
        """
        dst = mock.generate_url(transfer_time='1s', size_post='1M')
        src_bad = mock.generate_url(errno=125)
        src_ok = mock.generate_url(size='1M')

        job_id = self.client.submit(
            [{
                'sources': [src_bad, src_ok],
                'destinations': [dst],
                'selection_strategy': 'orderly'
            }]
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])
        job = self.client.get(job_id, list_files=True)
        self.assertEqual('CANCELED', job['files'][0]['file_state'])
        self.assertEqual('FINISHED', job['files'][1]['file_state'])


if __name__ == '__main__':
    unittest.main()
