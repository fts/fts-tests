#!/usr/bin/env python
import logging
import os
import stat
import sys
import threading
import time
from optparse import OptionParser

import fts3.rest.client.easy as fts3
import gfal2
from dirq.QueueSimple import QueueSimple

DEFAULT_SOURCE_DIR = 'gsiftp://na62merger2.cern.ch/merger/cdr'
DEFAULT_DEST_DIR = 'gsiftp://eospublicftp.cern.ch/eos/workspace/f/fts/test/na62'

log = logging.getLogger(__name__)


def _get_default_proxy():
    user_proxy = "/tmp/x509up_u%d" % os.getuid()
    return os.getenv('X509_USER_PROXY', user_proxy)


def _setup_logging(debug):
    log_level = logging.INFO
    if debug:
        log_level = logging.DEBUG

    logging.basicConfig(level=log_level, format='%(levelname)s %(message)s')
    if sys.stdout.isatty():
        logging.addLevelName(logging.DEBUG, "\033[1;2m%-8s\033[1;m" % logging.getLevelName(logging.DEBUG))
        logging.addLevelName(logging.INFO, "\033[1;34m%-8s\033[1;m" % logging.getLevelName(logging.INFO))
        logging.addLevelName(logging.ERROR, "\033[1;31m%-8s\033[1;m" % logging.getLevelName(logging.ERROR))
        logging.addLevelName(logging.WARNING, "\033[1;33m%-8s\033[1;m" % logging.getLevelName(logging.WARNING))


def _filesize(size):
    """
    Readable filesize 
    """
    if size >= 1024 ** 3:
        return "%.2fGB" % (size / 1024.0 ** 3)
    elif size >= 1024 ** 2:
        return "%.2fMB" % (size / 1024.0 ** 2)
    elif size >= 1024:
        return "%.2fKB" % (size / 1024.0)
    return "%d bytes" % size


class Poller(threading.Thread):
    """
    Polls FTS3 for job statuses
    """

    def __init__(self, fts3_endpoint, ucert, poll_queue_path, sleep_time):
        """
        Constructor
        """
        super(Poller, self).__init__()
        self.dirq = QueueSimple(poll_queue_path)
        self.sleep_time = sleep_time
        self.fts3_context = fts3.Context(fts3_endpoint, ucert=ucert)

    def run(self):
        """
        Triggered by Thread.start()
        """
        log.info("Starting poller")
        while True:
            log.info("Poll")
            for item in self.dirq:
                if not self.dirq.lock(item):
                    continue

                job_id = self.dirq.get(item)
                self.dirq.remove(item)

                job_status = fts3.get_job_status(self.fts3_context, job_id)
                log.info("%s %s" % (job_id, job_status['job_state']))
                if job_status['job_state'] not in ('FINISHED', 'FAILED', 'CANCELED', 'FINISHEDDIRTY'):
                    log.debug("Re-enqueue %s" % job_id)
                    self.dirq.add(job_id)

            self.dirq.purge()
            time.sleep(self.sleep_time)


class Submitter(threading.Thread):
    """
    Submits one transfer per file found
    """

    def __init__(self, fts3_endpoint, ucert, poll_queue_path, submit_sleep, source, destination):
        """
        Constructor
        """
        super(Submitter, self).__init__()
        self.dirq = QueueSimple(poll_queue_path)
        self.sleep_time = submit_sleep
        self.fts3_context = fts3.Context(fts3_endpoint, ucert=ucert)
        self.gfal2_context = gfal2.creat_context()
        self.gfal2_context.set_opt_string("X509", "PROXY", ucert)
        self.source = source
        self.destination = destination

    def _submit(self, entry):
        """
        Submit a transfer
        """
        filename = entry[0].d_name
        source_url = self.source + '/' + filename
        dest_url = self.destination + '/' + filename
        log.info("%s %s => %s" % (_filesize(entry[1].st_size), source_url, dest_url))
        transfer = fts3.new_transfer(source_url, dest_url, filesize=entry[1].st_size)
        job = fts3.new_job([transfer], verify_checksum=True, overwrite=True)
        job_id = fts3.submit(self.fts3_context, job)
        log.info("Submitted %s" % job_id)
        self.dirq.add(job_id)

    def run(self):
        """
        Triggered by Thread.start()
        """
        log.info("Starting submitter")
        while True:
            fd = self.gfal2_context.opendir(self.source)
            entry = fd.readpp()
            while entry[0] is not None:
                if entry[0].d_name[0] == '.':
                    pass
                elif stat.S_ISDIR(entry[1].st_mode):
                    log.debug("Skip %s", entry[0].d_name)
                else:
                    self._submit(entry)
                entry = fd.readpp()
            time.sleep(self.sleep_time)


if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option('--fts3', type=str, help='FTS3 endpoint', default='https://fts3-daq.cern.ch:8446')
    parser.add_option('--proxy', type=str, help='User proxy', default=_get_default_proxy())
    parser.add_option('--queue', type=str, help='Dirq base location', default='/tmp/submit')
    parser.add_option('--debug', action='store_true', help='Enable debug output')
    parser.add_option('--poll-sleep', type=int, help='Seconds to sleep between poll iterations', default=30)
    parser.add_option('--submit-sleep', type=int, help='Seconds to sleep between submitting all files', default=600)
    parser.add_option('--no-submit', action='store_true', help='Disable submission')
    opt, args = parser.parse_args()

    source = DEFAULT_SOURCE_DIR
    destination = DEFAULT_DEST_DIR
    if len(args) > 1:
        destination = args[1]
    if len(args) > 0:
        source = args[0]

    _setup_logging(opt.debug)

    # Different queue paths
    poll_queue_path = os.path.join(opt.queue, 'poll')

    # Poller thread
    poller = Poller(opt.fts3, opt.proxy, poll_queue_path, opt.poll_sleep)
    poller.daemon = True
    poller.start()

    # Listing and submiting
    if not opt.no_submit:
        submitter = Submitter(opt.fts3, opt.proxy, poll_queue_path, opt.submit_sleep, source, destination)
        submitter.daemon = True
        submitter.start()

    # Stay busy
    while True:
        time.sleep(1)
