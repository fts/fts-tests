#!/usr/bin/env bash
#
# Helper script to run the tests installing all the dependencies and the like
#

# Default location
if [ -z "${VIRTUALENV}" ]; then
    VIRTUALENV="/tmp/fts-tests"
fi

# Create virtualenv
rm -rf "${VIRTUALENV}"
python3 -m venv "${VIRTUALENV}"

# Install dependencies
source "${VIRTUALENV}/bin/activate"
pip install --upgrade pip
pip install "git+https://gitlab.cern.ch/fts/fts-rest-flask.git@develop"
pip install "pytest"
pip install "stomp.py"

# Load system certificates
export REQUESTS_CA_BUNDLE=/etc/ssl/certs/ca-bundle.crt

# Disable ActiveMQ messaging until stomp.py usage is updated
# TODO: Update stomp.py usage for Python3
export FTS3_ENABLE_MSG=False

# Run tests
py.test -v --junitxml results.xml fts_*.py
