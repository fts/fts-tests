#!/usr/bin/env python3
import unittest

from lib import TestCaseBase, config
from lib import mock_plugin_helper as mock


class TestCancel(TestCaseBase):
    """
    Test transfer cancellation
    """

    def test_cancel(self):
        """
        Cancelling a file that is still active/submitted must succeed
        """
        src, dst = mock.generate_pair(size='1M', transfer_time='10s')

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
        )
        self.client.cancel(job_id)

        job = self.client.wait_for(job_id)
        self.assertEqual('CANCELED', job['job_state'])

    def test_cancel_terminal(self):
        """
        Cancelling a file that is in a terminal state
        """
        src, dst = mock.generate_pair(size='1M', transfer_time='1s')

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        self.client.cancel(job_id)
        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msgs))

    def test_cancel_while_active(self):
        """
        Cancel the file while it is active. Regression test for FTS-273
        """
        src, dst = mock.generate_pair(size='1M', transfer_time='30s')

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
        )
        self.client.wait_for(job_id, state_in=['ACTIVE'])
        self.client.cancel(job_id)
        job = self.client.wait_for(job_id)

        self.assertEqual('CANCELED', job['job_state'])

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msgs))
            self.assertEqual('Abort', job_msgs[0]['t_final_transfer_state'])

    def test_cancel_file_granularity(self):
        """
        Cancel only a subset of a job
        """
        if config.ClientImpl == 'lib.cli':
            return  # SkipTest not available in el6

        pairs = [
            mock.generate_pair(size='1M', transfer_time='30s'),
            mock.generate_pair(size='1M', transfer_time='30s')
        ]

        transfers = [{'sources': [transfer_pair[0]], 'destinations': [transfer_pair[1]]} for transfer_pair in pairs]

        job_id = self.client.submit(transfers)
        job = self.client.get(job_id, list_files=True)
        self.client.cancel(job_id, [job['files'][0]['file_id']])
        self.client.wait_for(job_id)

        job = self.client.get(job_id, list_files=True)
        # Might be failed if started to run before we cancelled
        self.assertIn(job['files'][0]['file_state'], ('CANCELED', 'FAILED'))
        self.assertEqual('FINISHED', job['files'][1]['file_state'])

    def test_cancel_missing(self):
        """
        Try to cancel a job that doesn't exist
        """
        try:
            self.client.cancel('d3085936-c5bf-11e5-97a1-02163e006dd0')
            self.fail('This should have failed!')
        except:
            pass


if __name__ == '__main__':
    unittest.main()
