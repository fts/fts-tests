#!/usr/bin/env python3
import unittest

from lib import TestCaseBase
from lib import mock_plugin_helper as mock
from fts3.rest.client import ClientError


class TestUserChecksum(TestCaseBase):
    """
    Test transfers with user provided checksum
    """

    def test_with_user_checksum_ok(self):
        """
        Ask to validate the checksum and provide the right checksum
        """
        src, dst = mock.generate_pair(checksum='1a2b3c')

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst], 'checksum': 'adler32:1a2b3c'}],
            verify_checksum=True
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        self.assertEqual('FINISHED', job['files'][0]['file_state'])

        if self.messaging:
            job_msg = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msg))
            self.assertEqual('Ok', job_msg[0]['t_final_transfer_state'])

    def test_with_user_checksum_fail(self):
        """
        Ask to validate the checksum but give a bad one on purpose
        """
        src, dst = mock.generate_pair(checksum='1a2b3c')

        job_id = self.client.submit(
            [{
                'sources': [src],
                'destinations': [dst],
                'checksum': 'adler32:000000'
            }],
            verify_checksum=True
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FAILED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        self.assertEqual('FAILED', job['files'][0]['file_state'])

        if self.messaging:
            job_msg = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msg))
            self.assertEqual('Error', job_msg[0]['t_final_transfer_state'])
            
    def test_with_user_checksum_fail_false_verify(self):
        """
        Ask not to validate the checksum but give a bad one on purpose
        """
        src, dst = mock.generate_pair(checksum='1a2b3c')

        job_id = self.client.submit(
            [{
                'sources': [src],
                'destinations': [dst],
                'checksum': 'adler32:000000'
            }],
            verify_checksum=False
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FAILED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        self.assertEqual('FAILED', job['files'][0]['file_state'])

        if self.messaging:
            job_msg = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msg))
            self.assertEqual('Error', job_msg[0]['t_final_transfer_state'])
            
    def test_with_user_checksum_fail_verify_source(self):
        """
        Ask to validate the checksum but give a bad one on purpose and verify per source
        """
        src, dst = mock.generate_pair(checksum='1a2b3c')

        job_id = self.client.submit(
            [{
                'sources': [src],
                'destinations': [dst],
                'checksum': 'adler32:000000'
            }],
            verify_checksum='source'
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FAILED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        self.assertEqual('FAILED', job['files'][0]['file_state'])

        if self.messaging:
            job_msg = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msg))
            self.assertEqual('Error', job_msg[0]['t_final_transfer_state'])
    
    def test_with_user_checksum_fail_verify_target(self):
        """
        Ask to validate the checksum but give a bad one on purpose and verify per target
        """
        src, dst = mock.generate_pair(checksum='1a2b3c')

        job_id = self.client.submit(
            [{
                'sources': [src],
                'destinations': [dst],
                'checksum': 'adler32:000000'
            }],
            verify_checksum='target'
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FAILED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        self.assertEqual('FAILED', job['files'][0]['file_state'])

        if self.messaging:
            job_msg = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msg))
            self.assertEqual('Error', job_msg[0]['t_final_transfer_state'])
    
    def test_with_user_checksum_fail_verify_both(self):
        """
        Ask to validate the checksum but give a bad one on purpose and verify per both
        """
        src, dst = mock.generate_pair(checksum='1a2b3c')

        job_id = self.client.submit(
            [{
                'sources': [src],
                'destinations': [dst],
                'checksum': 'adler32:000000'
            }],
            verify_checksum='target'
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FAILED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        self.assertEqual('FAILED', job['files'][0]['file_state'])

        if self.messaging:
            job_msg = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msg))
            self.assertEqual('Error', job_msg[0]['t_final_transfer_state'])
            
    def test_with_user_checksum_fail_no_verify_absent(self):
        """
        Absent validation of the checksum but give a bad one on purpose
        """
        src, dst = mock.generate_pair(checksum='1a2b3c')

        job_id = self.client.submit(
            [{
                'sources': [src],
                'destinations': [dst],
                'checksum': 'adler32:000000'
            }],
            
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FAILED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        self.assertEqual('FAILED', job['files'][0]['file_state'])

        if self.messaging:
            job_msg = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msg))
            self.assertEqual('Error', job_msg[0]['t_final_transfer_state'])
    
    def test_with_user_checksum_fail_no_verify(self):
        """
        Ask to validate the checksum but give a bad one on purpose
        """
        src, dst = mock.generate_pair(checksum='1a2b3c')

        job_id = self.client.submit(
            [{
                'sources': [src],
                'destinations': [dst],
                'checksum': 'adler32:000000'
            }],
            verify_checksum='none'
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        self.assertEqual('FINISHED', job['files'][0]['file_state'])

        if self.messaging:
            job_msg = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msg))
            self.assertEqual('Ok', job_msg[0]['t_final_transfer_state'])
    
    def test_with_user_checksum_ok_source(self):
        """
        Ask to validate the checksum and provide the right checksum and verify source
        """
        src, dst = mock.generate_pair(checksum='1a2b3c')

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst], 'checksum': 'adler32:1a2b3c'}],
            verify_checksum='source'
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        self.assertEqual('FINISHED', job['files'][0]['file_state'])

        if self.messaging:
            job_msg = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msg))
            self.assertEqual('Ok', job_msg[0]['t_final_transfer_state'])
    
    def test_with_user_checksum_ok_target(self):
        """
        Ask to validate the checksum and provide the right checksum and verify target
        """
        src, dst = mock.generate_pair(checksum='1a2b3c')

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst], 'checksum': 'adler32:1a2b3c'}],
            verify_checksum='target'
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        self.assertEqual('FINISHED', job['files'][0]['file_state'])

        if self.messaging:
            job_msg = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msg))
            self.assertEqual('Ok', job_msg[0]['t_final_transfer_state'])
    
    def test_with_user_checksum_ok_both(self):
        """
        Ask to validate the checksum and provide the right checksum and verify both
        """
        src, dst = mock.generate_pair(checksum='1a2b3c')

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst], 'checksum': 'adler32:1a2b3c'}],
            verify_checksum='both'
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        self.assertEqual('FINISHED', job['files'][0]['file_state'])

        if self.messaging:
            job_msg = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msg))
            self.assertEqual('Ok', job_msg[0]['t_final_transfer_state'])
    
    def test_with_user_checksum_ok_none(self):
        """
        Ask to validate the checksum and provide the right checksum and no verify
        """
        src, dst = mock.generate_pair(checksum='1a2b3c')

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst], 'checksum': 'adler32:1a2b3c'}],
            verify_checksum='none'
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        self.assertEqual('FINISHED', job['files'][0]['file_state'])

        if self.messaging:
            job_msg = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msg))
            self.assertEqual('Ok', job_msg[0]['t_final_transfer_state'])
            
    def test_without_user_checksum_source(self):
        """
        Ask to validate the checksum by source and non provide checksum
        """
        src, dst = mock.generate_pair(checksum='1a2b3c')

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            verify_checksum='source'
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FAILED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        self.assertEqual('FAILED', job['files'][0]['file_state'])

        if self.messaging:
            job_msg = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msg))
            self.assertEqual('Error', job_msg[0]['t_final_transfer_state'])
            
    def test_without_user_checksum_target(self):
        """
        Ask to validate the checksum by target and non provide checksum 
        """
        src, dst = mock.generate_pair(checksum='1a2b3c')
  
        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            verify_checksum='target'
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FAILED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        self.assertEqual('FAILED', job['files'][0]['file_state'])

        if self.messaging:
            job_msg = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msg))
            self.assertEqual('Error', job_msg[0]['t_final_transfer_state'])
    
    def test_without_user_checksum_both(self):
        """
        Ask for checksum validation "both" and do not provide checksum
        """
        src, dst = mock.generate_pair(checksum='1a2b3c')

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            verify_checksum='both'
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        self.assertEqual('FINISHED', job['files'][0]['file_state'])

        if self.messaging:
            job_msg = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msg))
            self.assertEqual('Ok', job_msg[0]['t_final_transfer_state'])
            
    def test_without_user_checksum_none(self):
        """
        Do not ask for checksum validation and do not provide checksum
        """
        src, dst = mock.generate_pair(checksum='1a2b3c')

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            verify_checksum='none'
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        self.assertEqual('FINISHED', job['files'][0]['file_state'])

        if self.messaging:
            job_msg = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msg))
            self.assertEqual('Ok', job_msg[0]['t_final_transfer_state'])

    def test_without_user_checksum_and_provide_an_invalid_verification(self):
        """
        Ask to validate with a non-existing verification and non provide checksum
        """
        src, dst = mock.generate_pair(checksum='1a2b3c')
        try:
            self.client.submit(
                [{'sources': [src], 'destinations': [dst]}],
                verify_checksum='error'
            )
            self.fail('Submission should have failed, wrong verification')
        except ClientError:
            pass
        

if __name__ == '__main__':
    unittest.main()
