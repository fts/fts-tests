#!/usr/bin/env python3
import logging
import time
import unittest

from lib import TestCaseBase
from lib import mock_plugin_helper as mock


class TestMaxTimeInQueue(TestCaseBase):
    """
    Test max time in queue
    """

    SE = 'expires.cern.ch'
    SE2 = 'not-expires.cern.ch'

    def tearDown(self):
        """
        Clean up
        """
        self.client.unban_se('mock://' + TestMaxTimeInQueue.SE)

    @TestCaseBase.skip_for_short_run
    def test_max_time_finish_first(self):
        """
        Submit a simple job. Max time in queue will be way longer than time to execute,
        so it must finish
        """
        src, dst = mock.generate_pair()

        job_id = self.client.submit([{'sources': [src], 'destinations': [dst]}], max_time_in_queue=3600)

        # Validate polling
        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

    @TestCaseBase.skip_for_short_run
    def test_max_time_expire_during(self):
        """
        Do not cancel if already running
        """
        src, dst = mock.generate_pair(transfer_time='10s')

        job_id = self.client.submit([{'sources': [src], 'destinations': [dst]}], max_time_in_queue='5s')

        # Validate polling
        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

    @TestCaseBase.skip_for_short_run
    def test_max_time_expires(self):
        """
        Make sure at least one expires
        """
        # Stop transfers, so it gets stuck in submitted
        self.client.ban_se('mock://' + TestMaxTimeInQueue.SE, status='WAIT', allow_submit=True)

        src, dst = mock.generate_pair(source_host=TestMaxTimeInQueue.SE, transfer_time='10s')
        job_id = self.client.submit([{'sources': [src], 'destinations': [dst]}], max_time_in_queue='5s')

        job = self.client.get(job_id)
        self.assertEqual(job['job_state'], 'SUBMITTED')

        # FTS3 only checks every 300 seconds, so try once in a while during this
        # window
        logging.getLogger(__name__).warning('This could take up to 5 minutes')
        wait = 360
        while wait:
            job = self.client.get(job_id)
            if job['job_state'] not in ('SUBMITTED', 'ACTIVE', 'READY', 'STAGING', 'STARTED'):
                break
            time.sleep(10)
            wait -= 10

        self.assertEqual(job['job_state'], 'CANCELED')

    @TestCaseBase.skip_for_short_run
    def test_max_time_expires_fine_grain(self):
        """
        Submit a job with two transfers. One must expire, the other must not.
        """
        # Stop transfers, so it gets stuck in submitted
        self.client.ban_se('mock://' + TestMaxTimeInQueue.SE, status='WAIT', allow_submit=True)

        pair1 = mock.generate_pair(source_host=TestMaxTimeInQueue.SE, transfer_time='10s')
        pair2 = mock.generate_pair(source_host=TestMaxTimeInQueue.SE2, transfer_time='10s')

        transfers = []
        transfers.append({'sources': [pair1[0]], 'destinations': [pair1[1]]})
        transfers.append({'sources': [pair2[0]], 'destinations': [pair2[1]]})

        job_id = self.client.submit(transfers, max_time_in_queue='5s')

        job = self.client.get(job_id)
        self.assertEqual(job['job_state'], 'SUBMITTED')

        # FTS3 only checks every 300 seconds, so try once in a while during this
        # window
        logging.getLogger(__name__).warning('This could take up to 5 minutes')
        wait = 360
        while wait:
            job = self.client.get(job_id)
            if job['job_state'] not in ('SUBMITTED', 'ACTIVE', 'READY', 'STAGING', 'STARTED'):
                break
            time.sleep(10)
            wait -= 10

        self.assertEqual(job['job_state'], 'CANCELED')
        for f in job['files']:
            if f['source_surl'] == pair1[0]:
                self.assertEqual(f['file_state'], 'CANCELED')
            else:
                self.assertEqual(f['file_state'], 'FINISHED')


if __name__ == '__main__':
    unittest.main()
