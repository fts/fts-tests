#!/usr/bin/env python3
import os
import logging
import signal
import unittest

from lib import TestCaseBase
from lib import mock_plugin_helper as mock


class TestUrlCopySignals(TestCaseBase):
    """
    Test transfers that raise signals
    """

    @TestCaseBase.skip_for_short_run
    def test_signal_segv(self):
        """
        Submit a transfer that will segfault the url_copy process
        """
        src, dst = mock.generate_pair(signal=signal.SIGSEGV)

        job_id = self.client.submit([{'sources': [src], 'destinations': [dst]}])

        self.client.wait_for(job_id)
        job = self.client.get(job_id)
        self.assertEqual('FAILED', job['job_state'])
        self.assertTrue(job['files'][0]['reason'].find('Transfer process died with') > -1)

        if self.messaging:
            job_msg = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msg))
            job_msg = job_msg[0]
            self.assertEqual('UNKNOWN', job_msg['job_state'])
            self.assertEqual(src, job_msg['src_url'])
            self.assertEqual(dst, job_msg['dst_url'])
            self.assertEqual('Error', job_msg['t_final_transfer_state'])
            # Validate state changes
            file_id = int(job_msg['tr_id'].split('__')[3])
            self.assertStateTransitions(job_id, file_id, 'SUBMITTED', 'READY', 'ACTIVE', 'FAILED')

        logging.info('Finished with %s' % job['job_state'])

    @TestCaseBase.skip_for_short_run
    def test_signal_abort(self):
        """
        Submit a transfer that will get a SIGABRT
        """
        src, dst = mock.generate_pair(signal=signal.SIGABRT)

        job_id = self.client.submit([{'sources': [src], 'destinations': [dst]}])

        self.client.wait_for(job_id)
        job = self.client.get(job_id)
        self.assertEqual('FAILED', job['job_state'])
        self.assertTrue(job['files'][0]['reason'].find('Transfer process died with') > -1)

        if self.messaging:
            job_msg = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msg))
            job_msg = job_msg[0]
            self.assertEqual('UNKNOWN', job_msg['job_state'])
            self.assertEqual(src, job_msg['src_url'])
            self.assertEqual(dst, job_msg['dst_url'])
            self.assertEqual('Error', job_msg['t_final_transfer_state'])
            # Validate state changes
            file_id = int(job_msg['tr_id'].split('__')[3])
            self.assertStateTransitions(job_id, file_id, 'SUBMITTED', 'READY', 'ACTIVE', 'FAILED')

        logging.info('Finished with %s' % job['job_state'])


if __name__ == '__main__':
    if not os.environ.get('NO_SIGNALS', None):
        unittest.main()
