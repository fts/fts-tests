#!/usr/bin/env python
import optparse
import os
import random
import fts3.rest.client.easy as fts3


def send_staging_requests(endpoint, storage, nhosts, bulksize, count, nfiles):
	context = fts3.Context(
		endpoint=endpoint,
		verify=True
	)

	for i in xrange(count):
		transfers = []
		for j in xrange(bulksize):
			surl = '%s-%s/path/file-%d?staging_time=%d' % (storage, random.randint(0, nhosts), random.randint(0, nfiles), random.randint(30,60))
			transfers.append(fts3.new_transfer(surl, surl))
		job = fts3.new_job(
			transfers, source_spacetoken='TEST',
			copy_pin_lifetime=1000, bring_online=3600
		)
		print "Job %d with %d files" % (i, len(job['files']))
		job_id = fts3.submit(context, job)
		print "\t%s" % job_id


if __name__ == '__main__':
	parser = optparse.OptionParser()
	parser.add_option('-s', '--endpoint', type=str, default='https://fts3-devel.cern.ch:8446')
	parser.add_option('-b', '--bulk', type=int, default=10)
	parser.add_option('-c', '--count', type=int, default=10)
	parser.add_option('-f', '--nfiles', type=int, default=500)
	parser.add_option('--se', type=str, default='mock://host')
        parser.add_option('-n', '--nhosts', type=int, default=20)

	opts, args = parser.parse_args()

	send_staging_requests(opts.endpoint, opts.se, opts.nhosts,  opts.bulk, opts.count, opts.nfiles)

