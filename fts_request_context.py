#!/usr/bin/env python3
import errno
import logging
import unittest

from lib import TestCaseBase
from lib import mock_plugin_helper as mock


class TestRequestContextSubmission(TestCaseBase):
    """
    Test simple submissions by using request context. Just a source and a destination.
    """

    @TestCaseBase.skip_for_short_run
    def test_submit_successful_requet_context(self):
        """
        Simplest job with source and destination pair, all goes good, nothing special
        """
        src, dst = mock.generate_pair()

        job_id = self.client.submit_request_context([{'sources': [src], 'destinations': [dst]}])

        # Validate polling
        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        if self.messaging:
            # Validate start message
            job_msg = self.messaging.get_job_start(job_id)
            self.assertEqual(1, len(job_msg))

            # Validate completion message
            job_msg = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msg))
            job_msg = job_msg[0]
            self.assertEqual('UNKNOWN', job_msg['job_state'])
            self.assertEqual(src, job_msg['src_url'])
            self.assertEqual(dst, job_msg['dst_url'])
            self.assertEqual('Ok', job_msg['t_final_transfer_state'])

        logging.info('Finished with %s' % job['job_state'])

    @TestCaseBase.skip_for_short_run
    def test_submit_source_enoent_request_context(self):
        """
        Source does not exist by using request context
        """
        src = mock.generate_url(errno=errno.ENOENT)
        dst = mock.generate_url(size_pre=-1, size_post='1M', transfer_time='2s')

        job_id = self.client.submit_request_context([{'sources': [src], 'destinations': [dst]}])

        # Validate polling
        job = self.client.wait_for(job_id)
        self.assertEqual('FAILED', job['job_state'])

        if self.messaging:
            # Validate start message
            job_msg = self.messaging.get_job_start(job_id)
            self.assertEqual(1, len(job_msg))

            # Validate completion message
            job_msg = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msg))
            job_msg = job_msg[0]
            self.assertEqual('UNKNOWN', job_msg['job_state'])
            self.assertEqual(src, job_msg['src_url'])
            self.assertEqual(dst, job_msg['dst_url'])
            self.assertEqual('Error', job_msg['t_final_transfer_state'])
            self.assertEqual('SOURCE', job_msg['tr_error_scope'])
            self.assertEqual('NO_SUCH_FILE_OR_DIRECTORY', job_msg['tr_error_category'])
            self.assertEqual(False, int(job_msg['is_recoverable']))

        logging.info('Finished with %s' % job['job_state'])


if __name__ == '__main__':
    unittest.main()
