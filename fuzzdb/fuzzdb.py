#!/usr/bin/env python
#
# FuzzDB inserts spurious entries into the DB.
# This is intended to test if the sanity checks work as intended, and can recover
# inconsistencies.
#

import logging
import random
import sys
import uuid
from datetime import datetime
from optparse import OptionParser
from fts3.model import *

log = logging.getLogger(__name__)


class Fuzzer(object):
    def __init__(self, conn):
        """
        Constructor
        :param conn: Database connection string
        """
        engine = sqlalchemy.create_engine(conn, isolation_level='READ COMMITTED')
        Session = sqlalchemy.orm.sessionmaker(bind=engine)
        self.db = Session()

    def _generate_transfers(self, job_id, options):
        """
        Generate the random transfers
        """
        files = []
        for i in xrange(random.randint(0, options.files)):
            file = File()
            file.job_id = job_id
            file.file_state = random.choice(options.file_states)
            if file.file_state in FileTerminalStates:
                file.job_finished = datetime.utcnow()
            if file.file_state in ('STARTED'):
                file.staging_start = datetime.utcnow()
            file.source_se = 'mock://a'
            file.dest_se = 'mock://b'
            file.source_surl = 'mock://a/path'
            file.dest_surl = 'mock://b/path'
            file.vo_name = 'dteam'

            files.append(file)
        return files

    def fill(self, options):
        """
        Generate and insert the transfers into the DB
        """
        for i in xrange(options.jobs):
            job = Job()
            job.job_id = uuid.uuid4()
            job.submit_time = datetime.utcnow()
            job.job_state = random.choice(options.job_states)
            if job.job_state in JobTerminalStates and random.randint(0, 10) > 2:
                job.job_finished = datetime.utcnow()
            job.vo_name = 'dteam'
            job.source_se = 'mock://a'
            job.dest_se = 'mock://b'
            job.user_dn = '/CN=mock'

            files = self._generate_transfers(job.job_id, options)

            log.info(
                'Generated job id %s %s (%s) with %d transfers' % (job.job_id, job.job_state, job.job_finished, len(files))
            )

            try:
                self.db.add(job)
                for file in files:
                    self.db.add(file)
                self.db.commit()
            except:
                self.db.rollback()
                raise


if __name__ == '__main__':
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.INFO)
    log.addHandler(handler)
    log.setLevel(logging.INFO)

    parser = OptionParser()
    parser.add_option('--jobs', type=int, default=100, help='Number of jobs to generate')
    parser.add_option('--files', type=int, default=5, help='Max number of transfers per job')
    parser.add_option('--file-states', type=str, action='append', default=FileTerminalStates + FileActiveStates,
                      help='Possible file states')
    parser.add_option('--job-states', type=str, action='append', default=JobActiveStates + JobTerminalStates,
                      help='Possible job states')

    opts, args = parser.parse_args()
    if len(args) < 1:
        parser.error('Missing database connection string')

    fuzzer = Fuzzer(args[0])
    fuzzer.fill(opts)
