#!/usr/bin/env python3
import os
import unittest

from lib import TestCaseBase
from lib import mock_plugin_helper as mock


class TestMultihop(TestCaseBase):
    """
    Test job with multihop transfers
    """

    def test_multihop(self):
        """
        Transfer multihop
        """
        src = mock.generate_url(size='1M')
        hop = mock.generate_url(size_post='1M', transfer_time='5s')
        dst = mock.generate_url(size_post='1M', transfer_time='5s')

        job_id = self.client.submit([
            {'sources': [src], 'destinations': [hop]},
            {'sources': [hop], 'destinations': [dst]}
        ], multihop=True)

        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        for f in job['files']:
            self.assertEqual('FINISHED', f['file_state'])

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id, expected=2)
            self.assertEqual(2, len(job_msgs))
            for msg in job_msgs:
                self.assertEqual('Ok', msg['t_final_transfer_state'])

    def test_multihop_hop_fails(self):
        """
        Transfer multihop, but the intermediate hop exists, overwrite is
        not passed, so the transfer must fail.
        """
        src = mock.generate_url(size='1M')
        hop = mock.generate_url(size_pre='1M', size_post='1M', transfer_time='5s')
        dst = mock.generate_url(size_post='1M', transfer_time='5s')

        job_id = self.client.submit([
            {'sources': [src], 'destinations': [hop]},
            {'sources': [hop], 'destinations': [dst]}
        ], multihop=True)

        job = self.client.wait_for(job_id)
        self.assertEqual('FAILED', job['job_state'])

        multihop_unused_state = os.environ.get('FTS_MULTIHOP_UNUSED_STATE', 'NOT_USED')
        job = self.client.get(job_id, list_files=True)
        for f in job['files']:
            self.assertIn(f['file_state'], [multihop_unused_state, 'FAILED'])

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id, expected=1)
            self.assertEqual(1, len(job_msgs))
            for msg in job_msgs:
                self.assertEqual('Error', msg['t_final_transfer_state'])

    def test_multihop_final_fail(self):
        """
        Transfer multihop. First hop must pass, but the final destination
        exists, so the last hop will fail, and, therefore, the full job.
        """
        src = mock.generate_url(size='1M')
        hop = mock.generate_url(size_post='1M', transfer_time='5s')
        dst = mock.generate_url(size_pre='1M', size_post='1M', transfer_time='5s')

        job_id = self.client.submit([
            {'sources': [src], 'destinations': [hop]},
            {'sources': [hop], 'destinations': [dst]}
        ], multihop=True)

        job = self.client.wait_for(job_id)
        self.assertEqual('FAILED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        for f in job['files']:
            if f['source_surl'] == src:
                self.assertEqual('FINISHED', f['file_state'])
            else:
                self.assertEqual('FAILED', f['file_state'])

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id, expected=2)
            self.assertEqual(2, len(job_msgs))
            for msg in job_msgs:
                if msg['src_url'] == src:
                    self.assertEqual('Ok', msg['t_final_transfer_state'])
                else:
                    self.assertEqual('Error', msg['t_final_transfer_state'])


if __name__ == '__main__':
    unittest.main()
