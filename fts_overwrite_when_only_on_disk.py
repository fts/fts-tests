#!/usr/bin/env python3

import os
import unittest

from lib import TestCaseBase
from lib import mock_plugin_helper as mock
from fts3.rest.client.exceptions import *


class TestOverwriteWhenOnlyOnDisk(TestCaseBase):
    """
    Test transfer jobs with "overwrite-when-only-on-disk" option
    """

    def test_overwrite_disk(self):
        """
        Enable overwrite-when-only-on-disk and submit a transfer for a file
        that does not exist. The transfer must succeed.
        """
        src, dst = mock.generate_pair(dest_host="tape-endpoint.cern.ch")
        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            overwrite_when_only_on_disk=True,
            archive_timeout=86400
        )
        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msgs))
            self.assertEqual('Ok', job_msgs[0]['t_final_transfer_state'])

    def test_overwrite_disk_no_archive_timeout(self):
        """
        Enable overwrite-when-only-on-disk and submit a transfer for a file
        without the archive-timeout parameter. The transfer must fail.
        """
        src, dst = mock.generate_pair()
        try:
            self.client.submit(
                [{'sources': [src], 'destinations': [dst]}],
                overwrite_when_only_on_disk=True
            )
            self.fail('Submission should have failed')
        except (ClientError, Exception):
            pass

    def test_overwrite_disk_tape_endpoint_not_configured(self):
        """
        Enable overwrite-when-only-on-disk and submit a transfer for a file
        where the target endpoint is not configured as tape endpoint. The transfer must fail.
        """
        src, dst = mock.generate_pair(dest_exists=True)
        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            overwrite_when_only_on_disk=True,
            archive_timeout=86400
        )
        job = self.client.wait_for(job_id)
        self.assertEqual('FAILED', job['job_state'])

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msgs))
            self.assertEqual('Error', job_msgs[0]['t_final_transfer_state'])
            self.assertEqual('DESTINATION', job_msgs[0]['tr_error_scope'])

    def test_overwrite_disk_destination_disk(self):
        """
        Enable overwrite-when-only-on-disk and submit a transfer for a file
        that exists already only on disk. The transfer must succeed.
        """
        src, dst = mock.generate_pair(dest_host="tape-endpoint.cern.ch",
                                      dest_exists=True, user_status="ONLINE")
        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            overwrite_when_only_on_disk=True,
            archive_timeout=86400
        )
        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msgs))
            self.assertEqual('Ok', job_msgs[0]['t_final_transfer_state'])

    def test_overwrite_disk_destination_tape(self):
        """
        Enable overwrite-when-only-on-disk and submit a transfer for a file
        that exists already on disk. The transfer must fail.
        """
        src, dst = mock.generate_pair(dest_host="tape-endpoint.cern.ch",
                                      dest_exists=True, user_status="NEARLINE")
        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            overwrite_when_only_on_disk=True,
            archive_timeout=86400
        )
        job = self.client.wait_for(job_id)
        self.assertEqual('FAILED', job['job_state'])

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msgs))
            self.assertEqual('Error', job_msgs[0]['t_final_transfer_state'])
            self.assertEqual('DESTINATION', job_msgs[0]['tr_error_scope'])

    def test_overwrite_disk_destination_disk_and_tape(self):
        """
        Enable overwrite-when-only-on-disk and submit a transfer for a file
        that exists already on both disk and tape. The transfer must fail.
        """
        src, dst = mock.generate_pair(dest_host="tape-endpoint.cern.ch",
                                      dest_exists=True, user_status="ONLINE_AND_NEARLINE")
        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            overwrite_when_only_on_disk=True,
            archive_timeout=86400
        )
        job = self.client.wait_for(job_id)
        self.assertEqual('FAILED', job['job_state'])

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msgs))
            self.assertEqual('Error', job_msgs[0]['t_final_transfer_state'])
            self.assertEqual('DESTINATION', job_msgs[0]['tr_error_scope'])

    def test_overwrite_disk_destination_not_disk_or_tape(self):
        """
        Enable overwrite-when-only-on-disk and submit a transfer for a file
        that exists but locality is neither disk nor tape. The transfer must fail.
        """
        src, dst = mock.generate_pair(dest_host="tape-endpoint.cern.ch",
                                      dest_exists=True, user_status="UNAVAILABLE")
        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            overwrite_when_only_on_disk=True,
            archive_timeout=86400
        )
        job = self.client.wait_for(job_id)
        self.assertEqual('FAILED', job['job_state'])

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msgs))
            self.assertEqual('Error', job_msgs[0]['t_final_transfer_state'])
            self.assertEqual('DESTINATION', job_msgs[0]['tr_error_scope'])

    def test_overwrite_disk_destination_no_locality(self):
        """
        Enable overwrite-when-only-on-disk and submit a transfer for a file
        that exists but cannot retrieve locality info. The transfer must fail.
        """
        src, dst = mock.generate_pair(dest_host="tape-endpoint.cern.ch",
                                      dest_exists=True)
        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            overwrite_when_only_on_disk=True,
            archive_timeout=86400
        )
        job = self.client.wait_for(job_id)
        self.assertEqual('FAILED', job['job_state'])

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msgs))
            self.assertEqual('Error', job_msgs[0]['t_final_transfer_state'])
            self.assertEqual('DESTINATION', job_msgs[0]['tr_error_scope'])

    def test_overwrite_disk_multihop(self):
        """
        Enable overwrite-when-only-on-disk and submit a multihop transfer for a file
        that does not exist. The transfer must succeed.
        """
        src = mock.generate_url(size="1M")
        hop = mock.generate_url(size_pre="1M", size_post="1M")
        dst = mock.generate_url(size_post="1M", host="tape-endpoint.cern.ch")
        job_id = self.client.submit(
            [
                {'sources': [src], 'destinations': [hop]},
                {'sources': [hop], 'destinations': [dst]}
            ],
            overwrite_when_only_on_disk=True,
            overwrite_hop=True,
            archive_timeout=86400,
            multihop=True
        )
        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id)
            self.assertEqual(2, len(job_msgs))
            self.assertEqual('Ok', job_msgs[1]['t_final_transfer_state'])

    def test_overwrite_disk_multihop_destination_disk(self):
        """
        Enable overwrite-when-only-on-disk and submit a multihop transfer for a file
        whose final destination exists only on disk. The transfer must succeed.
        """
        src = mock.generate_url(size="1M")
        hop = mock.generate_url(size_pre="1M", size_post="1M")
        dst = mock.generate_url(size_pre="1M", size_post="1M",
                                host="tape-endpoint.cern.ch", user_status="ONLINE")
        job_id = self.client.submit(
            [
                {'sources': [src], 'destinations': [hop]},
                {'sources': [hop], 'destinations': [dst]}
            ],
            overwrite_when_only_on_disk=True,
            overwrite_hop=True,
            archive_timeout=86400,
            multihop=True
        )
        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id)
            self.assertEqual(2, len(job_msgs))
            self.assertEqual('Ok', job_msgs[1]['t_final_transfer_state'])

    def test_overwrite_disk_multihop_destination_tape(self):
        """
        Enable overwrite-when-only-on-disk and submit a multihop transfer for a file
        whose final destination exists only on tape. The transfer must fail.
        """
        src = mock.generate_url(size="1M")
        hop = mock.generate_url(size_pre="1M", size_post="1M")
        dst = mock.generate_url(size_pre="1M", size_post="1M",
                                host="tape-endpoint.cern.ch", user_status="NEARLINE")
        job_id = self.client.submit(
            [
                {'sources': [src], 'destinations': [hop]},
                {'sources': [hop], 'destinations': [dst]}
            ],
            overwrite_when_only_on_disk=True,
            overwrite_hop=True,
            archive_timeout=86400,
            multihop=True
        )
        job = self.client.wait_for(job_id)
        self.assertEqual('FAILED', job['job_state'])

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id)
            self.assertEqual(2, len(job_msgs))
            self.assertEqual('Error', job_msgs[1]['t_final_transfer_state'])
            self.assertEqual('DESTINATION', job_msgs[1]['tr_error_scope'])

    def test_overwrite_disk_multihop_destination_disk_and_tape(self):
        """
        Enable overwrite-when-only-on-disk and submit a multihop transfer for a file
        whose final destination exists on both disk and tape. The transfer must fail.
        """
        src = mock.generate_url(size="1M")
        hop = mock.generate_url(size_pre="1M", size_post="1M")
        dst = mock.generate_url(size_pre="1M", size_post="1M",
                                host="tape-endpoint.cern.ch", user_status="ONLINE_AND_NEARLINE")
        job_id = self.client.submit(
            [
                {'sources': [src], 'destinations': [hop]},
                {'sources': [hop], 'destinations': [dst]}
            ],
            overwrite_when_only_on_disk=True,
            overwrite_hop=True,
            archive_timeout=86400,
            multihop=True
        )
        job = self.client.wait_for(job_id)
        self.assertEqual('FAILED', job['job_state'])

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id)
            self.assertEqual(2, len(job_msgs))
            self.assertEqual('Error', job_msgs[1]['t_final_transfer_state'])
            self.assertEqual('DESTINATION', job_msgs[1]['tr_error_scope'])


if __name__ == '__main__':
    if os.environ.get('FTS_OVERWRITE_DISK', None):
        unittest.main()
