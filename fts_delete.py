#!/usr/bin/env python3
import errno
import itertools
import unittest

from lib import TestCaseBase
from lib import mock_plugin_helper as mock


class TestDelete(TestCaseBase):
    """
    Test deletion jobs.
    """

    @TestCaseBase.skip_for_short_run
    def test_delete(self):
        """
        Deleting a file must succeed
        """
        urls = [
            mock.generate_url(size='1M'),
            mock.generate_url(size='1M')
        ]

        job_id = self.client.submit_delete_job(transfers=urls)
        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        for f in itertools.chain(job.get('dm', []), job.get('files', [])):
            self.assertEqual('FINISHED', f['file_state'])

    @TestCaseBase.skip_for_short_run
    def test_delete_failed(self):
        """
        Deleting a file must be failed. We are trying to delete a file which does NOT exist.
        """
        urls = [
            mock.generate_url(errno=errno.ENOENT),
            mock.generate_url(errno=errno.ENOENT)
        ]

        job_id = self.client.submit(deletion=urls)
        job = self.client.wait_for(job_id)
        self.assertEqual('FAILED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        for f in itertools.chain(job.get('dm', []), job.get('files', [])):
            self.assertEqual('FAILED', f['file_state'])

    @TestCaseBase.skip_for_short_run
    def test_delete_finished_dirty(self):
        """
        Deleting a file must FINISHEDDIRTY. We'll try to delete both existing and non-existing file.
        """
        urls = [
            mock.generate_url(size='1M'),
            mock.generate_url(errno=errno.ENOENT)
        ]

        job_id = self.client.submit(deletion=urls)
        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHEDDIRTY', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        for f in itertools.chain(job.get('dm', []), job.get('files', [])):
            if ('errno=%d' % errno.ENOENT) in f['source_surl']:
                self.assertEqual('FAILED', f['file_state'])
            else:
                self.assertEqual('FINISHED', f['file_state'])


if __name__ == '__main__':
    unittest.main()
