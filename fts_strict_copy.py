#!/usr/bin/env python3
import errno
import logging
import unittest

from lib import TestCaseBase
from lib import mock_plugin_helper as mock


class TestStrictCopy(TestCaseBase):
    """
    Test copies with/without strict copy.
    """

    def test_no_strict(self):
        """
        Destination stat will fail with something else rather than ENOENT
        Without strict copy, the transfer must fail
        """
        src = mock.generate_url(size='1M')
        dst = mock.generate_url(size_post='55M', transfer_time='1s')

        job_id = self.client.submit([{'sources': [src], 'destinations': [dst]}])

        # Validate polling
        job = self.client.wait_for(job_id)
        self.assertEqual('FAILED', job['job_state'])

        logging.info('Finished with %s' % job['job_state'])

    def test_strict(self):
        """
        Destination stat will fail with something else rather than ENOENT
        With strict copy, the transfer must succeedl
        """
        src = mock.generate_url(size='1M')
        dst = mock.generate_url(size_post='55M', transfer_time='1s')

        job_id = self.client.submit([{'sources': [src], 'destinations': [dst]}], strict_copy=True)

        # Validate polling
        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        logging.info('Finished with %s' % job['job_state'])


if __name__ == '__main__':
    unittest.main()
