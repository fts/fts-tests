#!/usr/bin/env python3
import unittest

from lib import TestCaseBase
from lib import mock_plugin_helper as mock


class TestArchiving(TestCaseBase):
    """
    Test archiving files to tape
    """

    def test_archiving(self):
        """
        Do a transfer enabling archive monitoring
        """
        src = mock.generate_url(size='1M')
        dst = mock.generate_url(size_post='1M', transfer_time='2s')

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst], 'filesize':1024}], archive_timeout=120
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msgs))

            # Validate is_archiving flag on transfer completion messages
            self.assertEqual(True, job_msgs[0]["is_archiving"])

            # Validate state changes
            file_id = int(job_msgs[0]['tr_id'].split('__')[3])
            self.assertStateTransitions(job_id, file_id, 'SUBMITTED', 'READY', 'ACTIVE', 'ARCHIVING', 'FINISHED')
            self.assertArchivingTransitions(job_id, file_id, 'FINISHED')

    """
    Test simple submissions. Just a source and a destination.
    Should not go to ARCHIVING state and should not send is_archiving flag to ActiveMQ
    """
    def test_no_archiving(self):
        """
        Job with source and destination pair. No archiving
        """
        src = mock.generate_url(size='1M')
        dst = mock.generate_url(size_post='1M', transfer_time='2s')

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst], 'filesize':1024}]
        )

        # Validate polling
        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        # Validate messages
        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msgs))

            # Validate is_archiving flag on transfer completion messages
            self.assertEqual(False, job_msgs[0]["is_archiving"])

            # Validate completion message
            self.assertEqual(src, job_msgs[0]['src_url'])
            self.assertEqual(dst, job_msgs[0]['dst_url'])
            self.assertEqual('Ok', job_msgs[0]['t_final_transfer_state'])

            # Validate state changes
            file_id = int(job_msgs[0]['tr_id'].split('__')[3])
            self.assertStateTransitions(job_id, file_id, 'SUBMITTED', 'READY', 'ACTIVE', 'FINISHED')


if __name__ == '__main__':
    unittest.main()
