#!/usr/bin/env python3
import unittest

from lib import TestCaseBase
from lib import mock_plugin_helper as mock


class TestChecksum(TestCaseBase):
    """
    Test transfers with checksum
    """

    def test_simple_checksum_ok(self):
        """
        Just enable checksum
        """
        src, dst = mock.generate_pair(checksum='1a2b3c')

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            verify_checksum=True
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        self.assertEqual('FINISHED', job['files'][0]['file_state'])

        if self.messaging:
            job_msg = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msg))
            self.assertEqual('Ok', job_msg[0]['t_final_transfer_state'])

    def test_simple_checksum_fail(self):
        """
        Just enable checksum
        """
        src = mock.generate_url(size='1M', checksum='1a2b3c')
        dst = mock.generate_url(size_post='1M', checksum='000000')

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            verify_checksum=True
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FAILED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        self.assertEqual('FAILED', job['files'][0]['file_state'])

        if self.messaging:
            job_msg = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msg))
            self.assertEqual('Error', job_msg[0]['t_final_transfer_state'])

    def test_checkum_disable(self):
        """
        Disable checksum. Should finish even if checksums are different.
        """
        src = mock.generate_url(size='1M', checksum='1a2b3c')
        dst = mock.generate_url(size_post='1M', checksum='000000')

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            verify_checksum=False
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        self.assertEqual('FINISHED', job['files'][0]['file_state'])

        if self.messaging:
            job_msg = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msg))
            self.assertEqual('Ok', job_msg[0]['t_final_transfer_state'])
            
    
    def test_both_checksum_ok(self):
        """
        Verify both enable checksum
        """
        src, dst = mock.generate_pair(checksum='1a2b3c')

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            verify_checksum='both'
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        self.assertEqual('FINISHED', job['files'][0]['file_state'])

        if self.messaging:
            job_msg = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msg))
            self.assertEqual('Ok', job_msg[0]['t_final_transfer_state'])
    
    def test_source_checksum_ok(self):
        """
        Verify source enable checksum
        """
        src, dst = mock.generate_pair(checksum='1a2b3c')

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            verify_checksum='source'
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FAILED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        self.assertEqual('FAILED', job['files'][0]['file_state'])

        if self.messaging:
            job_msg = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msg))
            self.assertEqual('Error', job_msg[0]['t_final_transfer_state'])
            
    def test_target_checksum_ok(self):
        """
        Verify target enable checksum
        """
        src, dst = mock.generate_pair(checksum='1a2b3c')

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            verify_checksum='target'
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FAILED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        self.assertEqual('FAILED', job['files'][0]['file_state'])

        if self.messaging:
            job_msg = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msg))
            self.assertEqual('Error', job_msg[0]['t_final_transfer_state'])
            
    def test_both_checksum_fail(self):
        """
        Verify both enable checksum fail
        """
        src = mock.generate_url(size='1M', checksum='1a2b3c')
        dst = mock.generate_url(size_post='1M', checksum='000000')

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            verify_checksum='both'
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FAILED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        self.assertEqual('FAILED', job['files'][0]['file_state'])

        if self.messaging:
            job_msg = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msg))
            self.assertEqual('Error', job_msg[0]['t_final_transfer_state'])
            
    def test_source_checksum_fail(self):
        """
        Verify source enable checksum fail
        """
        src = mock.generate_url(size='1M', checksum='1a2b3c')
        dst = mock.generate_url(size_post='1M', checksum='000000')

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            verify_checksum='source'
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FAILED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        self.assertEqual('FAILED', job['files'][0]['file_state'])

        if self.messaging:
            job_msg = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msg))
            self.assertEqual('Error', job_msg[0]['t_final_transfer_state'])
    
    def test_target_checksum_fail(self):
        """
        Verify target enable checksum fail
        """
        src = mock.generate_url(size='1M', checksum='1a2b3c')
        dst = mock.generate_url(size_post='1M', checksum='000000')

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            verify_checksum='target'
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FAILED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        self.assertEqual('FAILED', job['files'][0]['file_state'])

        if self.messaging:
            job_msg = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msg))
            self.assertEqual('Error', job_msg[0]['t_final_transfer_state'])
            
    def test_none_checksum_fail(self):
        """
        Non verify enable checksum fail
        """
        src = mock.generate_url(size='1M', checksum='1a2b3c')
        dst = mock.generate_url(size_post='1M', checksum='000000')

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            verify_checksum='none'
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        self.assertEqual('FINISHED', job['files'][0]['file_state'])

        if self.messaging:
            job_msg = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msg))
            self.assertEqual('Ok', job_msg[0]['t_final_transfer_state'])
    
    def test_absent_verify_checksum_fail(self):
        """
        Absent verify enable checksum fail
        """
        src = mock.generate_url(size='1M', checksum='1a2b3c')
        dst = mock.generate_url(size_post='1M', checksum='000000')

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        self.assertEqual('FINISHED', job['files'][0]['file_state'])

        if self.messaging:
            job_msg = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msg))
            self.assertEqual('Ok', job_msg[0]['t_final_transfer_state'])
    


if __name__ == '__main__':
    unittest.main()
