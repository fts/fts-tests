#!/usr/bin/env python3
import os
import errno
import unittest

from lib import TestCaseBase, config
from lib import mock_plugin_helper as mock
from fts3.rest.client.exceptions import ClientError


class TestBringOnline(TestCaseBase):
    """
    Test bringing files online
    """

    # Leave at least one staging test
    def test_bring_online(self):
        """
        Do a transfer enabling bring online
        """
        src = mock.generate_url(size='1M')
        dst = mock.generate_url(size_post='1M', transfer_time='2s')
        
        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst], 'filesize':1024}], bring_online=2700
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id)
            self.assertEqual(1, len(job_msgs))

            # Validate state changes
            file_id = int(job_msgs[0]['tr_id'].split('__')[3])
            self.assertStateTransitions(job_id, file_id, 'STAGING', 'STARTED', 'SUBMITTED', 'READY', 'ACTIVE', 'FINISHED')
            self.assertStagingTransitions(job_id, file_id, 'STARTED', 'SUBMITTED', 'READY', 'ACTIVE', 'FINISHED')
            self.assertStagingFileSize(job_id, file_id, 'STARTED', 'SUBMITTED', 'READY', 'ACTIVE', 'FINISHED')

    @TestCaseBase.skip_for_short_run
    def test_new_staging_job(self):
        """
        Staging a file must succeed
        """
        urls = [
            mock.generate_url(size='1M'),
            mock.generate_url(size='1M')
        ]

        job_id = self.client.submit_staging_job(transfers=urls, bring_online=2700)
        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        job = self.client.get(job_id, list_files=True)
        for f in job['files']:
            self.assertEqual('FINISHED', f['file_state'])

    @TestCaseBase.skip_for_short_run
    def test_new_staging_job_failed(self):
        """
        Staging a file must fail because bring_online is 0 and there is no copy_pin_lifetime
        """
        # The CLI fills the missing with a default, so this test would fail
        if config.ClientImpl == 'lib.cli':
            return

        urls = [
            mock.generate_url(size='1M'),
            mock.generate_url(size='1M')
        ]
        try:
            self.client.submit_staging_job(transfers=urls, bring_online=0)
            self.fail('Should have failed')
        except ClientError:
            pass

    @TestCaseBase.skip_for_short_run
    def test_bring_online_only(self):
        """
        Source and destination are the same, so only bring online is done
        """
        src = mock.generate_url(size='1M')

        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [src]}],
            bring_online=2700
        )

        job = self.client.wait_for(job_id)
        self.assertEqual('FINISHED', job['job_state'])

        if self.messaging:
            job_msgs = self.messaging.get_job_completion(job_id, timeout=10)
            self.assertEqual(0, len(job_msgs))

    @TestCaseBase.skip_for_short_run
    def test_bringonline_multiple(self):
        """
        Regression test
        Submit a bring online job with a file that exist and another that doesn't
        """
        enoent = mock.generate_url(host='myhost.cern.ch', staging_time='5s', staging_errno=errno.ENOENT)
        ok = mock.generate_url(host='myhost.cern.ch', size='1M', staging_time='5s', staging_errno=None)

        transfers = [
            {'sources': [enoent], 'destinations': [enoent]},
            {'sources': [ok], 'destinations': [ok]}
        ]
        job_id = self.client.submit(transfers, bring_online=2700)

        self.client.wait_for(job_id)
        job = self.client.get(job_id, list_files=True)

        self.assertEqual('FINISHEDDIRTY', job['job_state'])
        for f in job['files']:
            if f['source_surl'] == enoent:
                self.assertEqual('FAILED', job['files'][0]['file_state'])
            else:
                self.assertEqual('FINISHED', job['files'][1]['file_state'])

    @TestCaseBase.skip_for_short_run
    def test_bringonline_reuse_1(self):
        """
        Regression test for FTS-887
        Submit a bring online job with session reuse enabled. The failed staging must finish first.
        """
        enoent_src, enoent_dst = mock.generate_pair(
            source_host='myhost.cern.ch', dest_host='mydest.cern.ch', staging_time='1s', staging_errno=errno.ENOENT
        )
        ok_src, ok_dst = mock.generate_pair(
            source_host='myhost.cern.ch', dest_host='mydest.cern.ch', staging_time='16s', staging_errno=None
        )
        transfers = [
            {'sources': [enoent_src], 'destinations': [enoent_dst]},
            {'sources': [ok_src], 'destinations': [ok_dst]}
        ]
        job_id = self.client.submit(transfers, bring_online=120, reuse=True)

        self.client.wait_for(job_id)
        job = self.client.get(job_id, list_files=True)

        self.assertEqual('FINISHEDDIRTY', job['job_state'])
        for f in job['files']:
            if f['source_surl'] == enoent_src:
                self.assertEqual('FAILED', job['files'][0]['file_state'])
            else:
                self.assertEqual('FINISHED', job['files'][1]['file_state'])

    @TestCaseBase.skip_for_short_run
    def test_bringonline_reuse_2(self):
        """
        Regression test for FTS-887
        Submit a bring online job with session reuse enabled. The successful staging must finish first.
        """
        enoent_src, enoent_dst = mock.generate_pair(
            source_host='myhost.cern.ch', dest_host='mydest.cern.ch', staging_time='16s', staging_errno=errno.ENOENT
        )
        ok_src, ok_dst = mock.generate_pair(
            source_host='myhost.cern.ch', dest_host='mydest.cern.ch', staging_time='1s', staging_errno=None
        )
        transfers = [
            {'sources': [enoent_src], 'destinations': [enoent_dst]},
            {'sources': [ok_src], 'destinations': [ok_dst]}
        ]
        job_id = self.client.submit(transfers, bring_online=2700, reuse=True)

        self.client.wait_for(job_id)
        job = self.client.get(job_id, list_files=True)

        self.assertEqual('FINISHEDDIRTY', job['job_state'])
        for f in job['files']:
            if f['source_surl'] == enoent_src:
                self.assertEqual('FAILED', job['files'][0]['file_state'])
            else:
                self.assertEqual('FINISHED', job['files'][1]['file_state'])


if __name__ == '__main__':
    if not os.environ.get('NO_BRINGONLINE', None):
        unittest.main()
