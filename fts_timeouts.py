#!/usr/bin/env python3
import logging
import signal
import time
import unittest

from lib import TestCaseBase
from lib import mock_plugin_helper as mock


class TestTimeout(TestCaseBase):
    """
    Test timeouts
    """

    @TestCaseBase.skip_for_short_run
    def test_stalled(self):
        """
        Submit a transfer that will die without possibility to recover. FTS3 must abort it.
        """
        src, dst = mock.generate_pair(signal=signal.SIGKILL)

        job_id = self.client.submit([{'sources': [src], 'destinations': [dst]}])

        self.client.wait_for(job_id, state_in=['ACTIVE'])

        # FTS3 only checks every 300 seconds, so try once in a while during this
        # window
        logging.getLogger(__name__).warning('This could take up to 7 minutes')
        wait = 420
        while wait:
            job = self.client.get(job_id)
            if job['job_state'] not in ('SUBMITTED', 'ACTIVE', 'READY', 'STAGING', 'STARTED'):
                break
            time.sleep(10)
            wait -= 10

        # Must be failed
        self.assertEqual(job['job_state'], 'FAILED')

        # Note: FTS3 submits a state change, not a completion message, on these situations
        logging.info('Finished with %s' % job['job_state'])

    @TestCaseBase.skip_for_short_run
    def test_stalled_session_reuse(self):
        """
        Submit a session reuse job, let it die, FTS3 must mark all entries as FAILED.
        Regression for FTS-542
        """
        pair1 = mock.generate_pair(source_host='test.cern.ch', dest_host='test.somewhere.uk', signal=signal.SIGKILL)
        pair2 = mock.generate_pair(source_host='test.cern.ch', dest_host='test.somewhere.uk')
        transfers = []
        transfers.append({'sources': [pair1[0]], 'destinations': [pair1[1]]})
        transfers.append({'sources': [pair2[0]], 'destinations': [pair2[1]]})

        job_id = self.client.submit(transfers, reuse=True)
        self.client.wait_for(job_id, state_in=['ACTIVE'])

        # FTS3 only checks every 300 seconds, so try once in a while during this
        # window
        logging.getLogger(__name__).warning('This could take up to 7 minutes')
        wait = 420 
        while wait:
            job = self.client.get(job_id)
            if job['job_state'] not in ('SUBMITTED', 'ACTIVE', 'READY', 'STAGING', 'STARTED'):
                break
            time.sleep(10)
            wait -= 10

        # Must be failed
        self.assertEqual(job['job_state'], 'FAILED')

        # All files must be failed!
        for f in job['files']:
            self.assertEqual('FAILED', f['file_state'])

    @TestCaseBase.skip_for_short_run
    def test_stalled_second_session_reuse(self):
        """
        Submit a session reuse job. First one goes through, second dies.
        """
        pair1 = mock.generate_pair(source_host='test.cern.ch', dest_host='test.somewhere.uk')
        pair2 = mock.generate_pair(source_host='test.cern.ch', dest_host='test.somewhere.uk', signal=signal.SIGKILL)
        transfers = []
        transfers.append({'sources': [pair1[0]], 'destinations': [pair1[1]]})
        transfers.append({'sources': [pair2[0]], 'destinations': [pair2[1]]})

        job_id = self.client.submit(transfers, reuse=True)
        self.client.wait_for(job_id, state_in=['ACTIVE'])

        # FTS3 only checks every 300 seconds, so try once in a while during this
        # window
        logging.getLogger(__name__).warning('This could take up to 7 minutes')
        wait = 420
        while wait:
            job = self.client.get(job_id)
            if job['job_state'] not in ('SUBMITTED', 'ACTIVE', 'READY', 'STAGING', 'STARTED'):
                break
            time.sleep(10)
            wait -= 10

        self.assertEqual(job['job_state'], 'FINISHEDDIRTY')

        # All files must be failed!
        for f in job['files']:
            if f['source_surl'] == pair2[0]:
                self.assertEqual('FAILED', f['file_state'])
            else:
                self.assertEqual('FINISHED', f['file_state'])

    @TestCaseBase.skip_for_short_run
    def test_not_stalled_session_reuse(self):
        """
        Submit a session reuse job. It will not stall, but the second transfer will not enter for six minutes, so
        we make sure the stalled code is triggered. The READY transfer must *NOT* go to stalled.
        Make sure we do not break this fixing FTS-542
        """
        pair1 = mock.generate_pair(
            source_host='test.cern.ch', dest_host='test.somewhere.uk', size='1M', transfer_time='6m'
        )
        pair2 = mock.generate_pair(
            source_host='test.cern.ch', dest_host='test.somewhere.uk', size='1M', transfer_time='10s'
        )
        transfers = []
        transfers.append({'sources': [pair1[0]], 'destinations': [pair1[1]]})
        transfers.append({'sources': [pair2[0]], 'destinations': [pair2[1]]})

        job_id = self.client.submit(transfers, reuse=True)
        self.client.wait_for(job_id)
        job = self.client.get(job_id)
        # All files must be failed!
        for f in job['files']:
            self.assertEqual('FINISHED', f['file_state'])

    @TestCaseBase.skip_for_short_run
    def test_not_stalled_reuse(self):
        """
        Submit a session reuse with a timeout. The transfer will finish in time.
        Regression for FTS-560
        """
        src, dst = mock.generate_pair(transfer_time='12m')
        job_id = self.client.submit(
            [{'sources': [src], 'destinations': [dst]}],
            timeout=14*60, reuse=True)

        logging.getLogger(__name__).warning('This could take up to 12 minutes')
        wait = 13*60
        while wait:
            job = self.client.get(job_id)
            if job['job_state'] not in ('SUBMITTED', 'ACTIVE', 'READY', 'STAGING', 'STARTED'):
                break
            time.sleep(10)
            wait -= 10

        for f in job['files']:
            self.assertEqual('FINISHED', f['file_state'])

    @TestCaseBase.skip_for_short_run
    def test_stalled_in_ready(self):
        """
        Submit a transfer that will die before it is even set to ACTIVE
        Regression for FTS-990
        """
        src, dst = mock.generate_pair(signal_on_load=signal.SIGKILL)

        job_id = self.client.submit([{'sources': [src], 'destinations': [dst]}])

        self.client.wait_for(job_id, state_in=['ACTIVE'])

        # FTS3 only checks every 300 seconds, so try once in a while during this
        # window
        logging.getLogger(__name__).warning('This could take up to 7 minutes')
        wait = 420
        while wait:
            job = self.client.get(job_id)
            if job['job_state'] not in ('SUBMITTED', 'ACTIVE', 'READY', 'STAGING', 'STARTED'):
                break
            time.sleep(10)
            wait -= 10

        # Must be failed
        self.assertEqual(job['job_state'], 'FAILED')

        # Note: FTS3 submits a state change, not a completion message, on these situations
        logging.info('Finished with %s' % job['job_state'])

    @TestCaseBase.skip_for_short_run
    def test_stalled_in_ready_session_reuse(self):
        """
        Submit a session reuse job, let it die, FTS3 must mark all entries as FAILED.
        Regression for FTS-990
        """
        pair1 = mock.generate_pair(source_host='test.cern.ch', dest_host='test.somewhere.uk', signal_on_load=signal.SIGKILL)
        pair2 = mock.generate_pair(source_host='test.cern.ch', dest_host='test.somewhere.uk')
        transfers = []
        transfers.append({'sources': [pair1[0]], 'destinations': [pair1[1]]})
        transfers.append({'sources': [pair2[0]], 'destinations': [pair2[1]]})

        job_id = self.client.submit(transfers, reuse=True, metadata='MOCK_LOAD_TIME_SIGNAL9')
        self.client.wait_for(job_id, state_in=['READY'])

        # FTS3 only checks every 300 seconds, so try once in a while during this
        # window
        logging.getLogger(__name__).warning('This could take up to 7 minutes')
        wait = 420
        while wait:
            job = self.client.get(job_id)
            if job['job_state'] not in ('SUBMITTED', 'ACTIVE', 'READY', 'STAGING', 'STARTED'):
                break
            time.sleep(10)
            wait -= 10

        # Must be failed
        self.assertEqual(job['job_state'], 'FAILED')

        # All files must be failed!
        for f in job['files']:
            self.assertEqual('FAILED', f['file_state'])

if __name__ == '__main__':
    unittest.main()
