#!/usr/bin/env python
import logging
import unittest
import requests
import lib.config

from xml.etree import ElementTree

Namespaces = {
    "sls": "http://sls.cern.ch/SLS/XML/update"
}


class TestSLS(unittest.TestCase):
    """
    Test the SLS view.
    """

    def _get_body(self, url):
        print(url)
        handle = requests.Session()
        handle.verify = False
        buffer = handle.get(url=url)
        handle.close()
        return buffer.content

    def test_sls(self):
        """
        Make sure the SLS view is publishing sensible information
        """
        sls_body = self._get_body(lib.config.Fts3MonEndpoint + "/fts3/ftsmon/stats/servers?format=sls")
        root = ElementTree.fromstring(sls_body)

        ai = root.findall("sls:availabilityinfo", Namespaces)
        self.assertEqual(len(ai), 1)

        status = root.findall("sls:status", Namespaces)
        self.assertEqual(len(status), 1)
        self.assertEqual(status[0].text, "available")

        data = root.findall("sls:data", Namespaces)
        self.assertEqual(len(data), 1)

        for nv in data[0].findall("sls:numericvalue", Namespaces):
            name = nv.get("name")
            value = nv.text
            logging.info("%s = %s" % (name, value))
            if name == "host_count":
                self.assertGreater(int(value), 0)


if __name__ == '__main__':
    unittest.main()
